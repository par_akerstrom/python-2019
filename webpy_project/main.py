import web

# If "this URL" (=/anything/anything) is accessed, look for the index class
urls = (
    '/(.*)/(.*)', 'index' # URL is passed to class as arguments
)

# Tell python where we find our html files. This will be our resource directory
render = web.template.render("resources/")

# Run our app with tuple(?) or urls. Not sure what global means.
app = web.application(urls, globals())

# This is run if a matching URL is hit.
class index:
    def GET(self, name, age):
        # Returns a rendered version of "main.html", passing in variables
        # Variables are used by the html file. Note how they must match order.
        # No respect is paid to the names of the variables.
        return render.main(name, age)

if __name__ == "__main__":
    app.run()