import aiohttp
import asyncio
from classes.webpage import WebPage
 
pages_of_interest = ["http://google.com", "http://bing.com"]
 
webpages = [WebPage(page) for page in pages_of_interest]
 
for webpage in webpages:
    print(webpage.get_url())
 
async def main():
    async with aiohttp.ClientSession() as session:
        for webpage in webpages:
            html, status = await fetch2(session, webpage)
            webpage.set_html(html)
            webpage.set_status(status)
 
async def fetch2(session, page):
    async with session.get(page.get_url()) as response:
        return await response.text(), response.status
 
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
 
for webpage in webpages:
    print(webpage.get_status())
