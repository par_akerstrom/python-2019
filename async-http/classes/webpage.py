class WebPage:
    def __init__(self, url):
        self.url = url
        self.html = ""

    def get_url(self):
        return self.url
 
    def set_html(self, html):
        self.html = html

    def set_status(self, status):
        self.status = status

    def get_status(self):
        return self.status
 
    def get_html(self):
        return self.html
