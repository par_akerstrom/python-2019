# coding: utf-8
from csv import reader,DictReader
import csv

# Noob version with old-school "csv.reader". This method is not top row aware
with open('mycsv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count=0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are: {", ".join(row)}')
            line_count += 1
        else:
            print(f'\t{row[0]} works in teh {row[1]} department, and was born in {row[2]}.')
            line_count += 1
    print(f'processed {line_count} lines')
    

with open('mycsv') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=',')
    line_count=0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are: {", ".join(row)}')
            line_count += 1
        print(f'\t{row["name"]} works in teh {row["department"]} department, and was born in {row["birthday month"]}.')
        line_count += 1
