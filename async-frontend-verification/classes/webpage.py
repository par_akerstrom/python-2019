import requests
from bs4 import BeautifulSoup
import logging
logger = logging.getLogger('sts/syb frontend checker')
hdlr = logging.FileHandler('./frontend_check.log')
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)
 
 
class Webpage:
    """
     webpage collection for frontend testing purposes
 
    Example: No current examples
 
    Attributes:
        name (str): name of customer
        url (str): url of customer
        product (str): product []
        region (str): Region where customer is. DR or no DR.
 
    Todo:
        * asyncio integration
 
    """
    def __init__(self, name, url, product, region):
        """ Declaration of instantiation variables """
        self.name = name
        self.url = url
        self.product = product
        self.region = region
        self.status = ""
 
    def who_am_i(self):
        """
        Displays who the customer is by print
        """
        print("name: {}\nurl: {}\nproduct: {}\nregion: {}\nstatus: {}\n".format(
            self.name, self.url, self.product, self.region, self.status))
        logger.info(
            "Printing info to user: name: %s url: %s product: %s status: %s",
            self.name,
            self.url,
            self.product,
            self.status)
 
    def get_url(self):
        """
        Returns: url (str): the url of the customer
        """
        return self.url
 
    def get_name(self):
        """
        Returns: name (str): the name of the customer
        """
        return self.name
 
    def set_status(self, status):
        """
        Sets the status of the customer
        Args: status (int): the HTTP response code
        """
        self.status = status
 
    def get_status(self):
        """
        Displays the status of the customer
        Returns: status (int): the HTTP response code
        """
        return self.status
 
    def get_region(self):
        """
        Displays the region of the customer
        Returns: region (str): the region of the customer
        """
        return self.region
 
    def connect_to_frontend(self):
       """
        Connects via HTTP/S to the customer's webpage
        Sets the response code as the customer's status
        """
        try:
            response = requests.get(self.url, timeout=10)
            logger.info(
                "URL: %s Status code: %s",
                self.url,
                response.status_code)
            self.set_status(response.status_code)
            return response.text
        except BaseException as e:
            logger.warning("Failed to connect. Error: %s", e)
            self.set_status(e)
 
    def asynch_connect_to_frontend(self, session, url):
        """
        Connects to multiple frontends via asynchronous calls
        """
        try:
            with session.get(url) as response:
                logger.info(
                    "URL: %s Status code: %s",
                    url,
                    response.status_code)
                self.set_status(response.status_code)
                return response.text
        except BaseException as e:
            logger.warning("Failed to connect. Error: %s", e)
 
    def make_soup(self):
        """
        Makes a bssoup of the customers webpage.
        The soup can be used for further parsing of webpage content
 
        Args:
            self (class object)
 
        Returns:
            soup (bs object): a beautifulsoup object
        """
        response = self.connect_to_frontend()
        # make the reply into a beautiful soup
        try:
            soup = BeautifulSoup(response.text, "html.parser")
        except BaseException as e:
            logger.warning("Failed to make soup. Error: %s", e)
        # Look for something on the page, such as the submit button
        # submit_button_result = soup.find(type="submit")
        # logger.info("Here's your submit button: %s", submit_button_result)
        return soup
