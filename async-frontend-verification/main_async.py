import asyncio
import requests
from concurrent.futures import ThreadPoolExecutor
import re
from classes.webpage import Webpage
from classes.bcolours import bcolours
import logging
from timeit import default_timer
 
logger = logging.getLogger('asynchronous frontend checker')
hdlr = logging.FileHandler('./frontend_check.log')
formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)
 
# Set search pattern for a customer name and region plus dr.
# Group one is name. Group two is region.
# search_pattern_customer = r"([A-Za-z]+)-([\w]+)-[A-Za-z]+"
# search_pattern_customer2 = r"([A-Za-z]+)-([\w-]+)"
search_pattern_customer = r"([A-Za-z-]+)-(emea-dr|us-dr|apac-dr|emea|us|apac)"
 
# Set search pattern for a full domain name
# #search_pattern_domain = r"([A-Za-z][\w\-]+\.[\w]+\.[\w\.]+)"
 
def read_bind_file():
        # Read Public DNS file from disk
        try:
                with open("<mydnsfileondisk>") as file:
                        lines = [line.rstrip('\n') for line in file]
                        logger.info("Loaded bind file")
                        print(bcolours.OKGREEN + "Bind file has been loaded successfully. Continuing." + bcolours.ENDC)
                return lines
        except BaseException as e:
                logger.error("Couldn't read bind file. Error: %s", e)
 
def filter_customers(lines):
        # Create a list of customers found in DNS file as per search pattern
        customers = [answer for answer in (re.match(search_pattern_customer, address) for address in lines) if answer is not None]
        logger.info("Count of customers: %s", len(customers))
        return customers
 
def create_class_objects(customers):
        # Create an object for each customer of the class "webpage"
        global class_objects
        class_objects = [Webpage(
                customer.group(1),
                "https://" + customer.group() + ".whatever.com",
                "product_name",
                str.upper(customer.group(2))) for customer in customers]
        return class_objects
 
 
def fetch(session, url):
        try:
                with session.get(url, timeout=10) as response:
                        logger.info("connected to %s", response.url)
                        status = response.status_code
                        text = response.text
                        url = response.url
                        elapsed_time = default_timer() - start_time
                        time_completed_at = "{:5.2f}s".format(elapsed_time)
                        logger.info("customer completed after: %s", time_completed_at)
                        return status, url
        except BaseException as e:
                logger.warning("Encountered error %s", e)
                return e, url
 
 
async def asynchronous_urls():
        urls = [u.get_url() for u in class_objects if u.get_region() == "US"]
        with ThreadPoolExecutor(max_workers=10) as executor:
                with requests.Session() as session:
 
                        loop = asyncio.get_event_loop()
 
                        tasks = [
                                loop.run_in_executor(
                                        executor,
                                        fetch,
                                        *(session, url)
                                        )
                                for url in urls
                        ]
 
                        for response in await asyncio.gather(*tasks):
                                try:
                                        if response:
                                                if response[0] == 200:
                                                        print("Status:" + bcolours.OKGREEN + " {} ".format(response[0]) +
                                                                bcolours.ENDC + "{}".format(response[1]))
                                                else:
                                                        print("Status:" + bcolours.FAIL + " {} ".format(response[0]) +
                                                                bcolours.ENDC + "{}".format(response[1]))
                                        else:
                                                print("Didn't get a response from the requests module")
                                except BaseException as e:
                                        print("You encountered exception {}".format(e))
                                        pass
 
 
def main():
        lines = read_bind_file()
        customers = filter_customers(lines)
        class_objects = create_class_objects(customers)
        logger.info("Starting asyncio event loop")
        loop = asyncio.get_event_loop()
        future = asyncio.ensure_future(asynchronous_urls())
        loop.run_until_complete(future)
 
 
start_time = default_timer()
main()
