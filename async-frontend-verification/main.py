import re
from classes.webpage import Webpage
from classes.bcolours import bcolours
from timeit import default_timer
import logging
logger = logging.getLogger('frontend checker')
hdlr = logging.FileHandler('./frontend_check.log')
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)
 
# Set search pattern for a customer name and region plus dr.
# Group one is name. Group two is region.
# search_pattern_customer = r"([A-Za-z]+)-([\w]+)-[A-Za-z]+"
# search_pattern_customer2 = r"([A-Za-z]+)-([\w-]+)"
search_pattern_customer = r"([A-Za-z-]+)-(emea-dr|us-dr|apac-dr|emea|us|apac)"
 
# Set search pattern for a full domain name
# #search_pattern_domain = r"([A-Za-z][\w\-]+\.[\w]+\.[\w\.]+)"
 
# Read Public DNS file from disk
try:
        with open("<mydnsfileondisk>") as file:
                lines = [line.rstrip('\n') for line in file]
except BaseException as e:
        print(e)
 
# Create a list of customers found in DNS file as per search pattern
customers = [answer for answer in (re.match(search_pattern_customer, address) for address in lines) if answer is not None]
 
# Create an object for each customer of the class "webpage"
class_objects = [Webpage(
        customer.group(1),
        "https://" + customer.group() + ".whatever.com",
        "product_name",
        str.upper(customer.group(2))) for customer in customers]
 
start_time = default_timer() # Set start time that we will measure from
 
# Connect to each webpage and get status
# Available regions: APAC / APAC-DR / EMEA / EMEA-DR / US / US-DR
for class_object in class_objects:
        if class_object.get_region() == "EMEA":
                class_object.connect_to_frontend()
                elapsed_time = default_timer() - start_time
                time_completed_at = "{:5.2f}".format(elapsed_time)
                logger.info("Customer completed at {}".format(time_completed_at))
                if class_object.get_status() == 200:
                        print("Status:" + bcolours.OKGREEN + " {} ".format(class_object.get_status()) + bcolours.ENDC + "Name: {}".format(class_object.get_name()))
                else:
                        print("Status:" + bcolours.FAIL + " {} ".format(class_object.get_status()) + bcolours.ENDC + "Name: {}".format(class_object.get_name()))

