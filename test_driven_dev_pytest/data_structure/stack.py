class Stack:
    def __init__(self):
        # Constructor method
        self._storage = [] # Why _? To signal internal variable.

    def __len__(self):
        # Magic Method for len
        return len(self._storage)

    def push(self, item):
        self._storage.append(item)

    def pop(self):
        try:
            item = self._storage.pop()
        except IndexError:
            item = None
        return item