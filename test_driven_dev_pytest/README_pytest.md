# Running Pytest
To simply run pytest we run python with the pytest module and we use the verbose switch
I guess that pytest will run all tests in the current test directory.
At least the ones that are named correctly.        
        
        python -m pytest -v
        
        ======================================================= test session starts =======================================================
        platform linux -- Python 3.7.3, pytest-4.5.0, py-1.8.0, pluggy-0.12.0 -- /home/par/.var/app/com.jetbrains.PyCharm-Community/data/virtualenvs/test_driven_dev_pytest-B1CM6DA9/bin/python
        cachedir: .pytest_cache
        rootdir: /home/par/PycharmProjects/python-2019/test_driven_dev_pytest
        collected 0 items                                                                                                                 
        
        ================================================== no tests ran in 0.05 seconds ===================================================
        
# Test Driven Development Basics
In TDD it's expected/best practice to write your tests prior
to writing your code. You define the tests and then make your code
pass the tests.

# Our Project - a Stack Data Structure
This is how a stack data structure looks like.
It can be compared to a stack of trays in a restaurant if we want.
We push trays on the stack and pop them off.

![stack_data_structure](stack_data_structure.png)

Our mission is to in a TDD fashion build and unit test functionality of
stack data structure. We must be able to push items onto a stack
and we need to be able to pop items from a stack.

# Assertions
assert is a keyword used for a test point. 
If the assertion is True, test passes, if False, test fails.
At any point in your test code, feel free to create assertions.

        def test_constructor():
            stack_object = Stack()  # Instantiate a stack object
            assert 1 == 2  # dummy test to show a failing assertion
            
            >       assert 1 == 2
            E       assert 1 == 2
            E         -1
            E         +2
            
            tests/test_stack.py:9: AssertionError

We can see that the AssertionError is thrown. Perfect, now we know
how a failure looks like.

So as we are to use TDD we will write another test that makes a bit
more sense, then we will fail it and then we will write our code to 
pass it. We will assert for the object type and the length of the object.

        def test_constructor():
            stack_object = Stack()  # Instantiate a stack object
            assert isinstance(stack_object, Stack)  # Check if stack_object is an instance of Stack.
            assert len(stack_object) == 0  # Check if the length of stack_object is 0.
            
            E       TypeError: object of type 'Stack' has no len()
            tests/test_stack.py:10: TypeError

Great, the length test failed. Why? Because our Stack class is empty at the moment
and it can't have a length yet (it's not a list or a tuple!, it's a class object).

So to make this test pass, we can define a magic method in our class to give the object
a len property

    class Stack:
        def __len__(self):
        # A magic method for len
        return 3

        E       assert 3 == 0
        E         -3
        E         +0
        
        tests/test_stack.py:10: AssertionError

OK great now our test returns new data! We're correctly capturing the "len" of our stack_object.
We can see that the assertion still fails because the int is not 0.
That's fine, we can fix that

We fix it by creating a constructor for our Stack class.
We give an attribute of an empty list that we will store values in later.
And lastly we make the magic method return the value of the list, which is empty.
The underscore on the storage is to visually signal that the developer intended for the
attribute to be internal and should not be used outside of the class, sort of. 

        class Stack:
            def __init__(self):
                # Constructor method
                self._storage = [] # Why _? To signal internal variable.

        def __len__(self):
            # Magic Method for len
            return len(self._storage)

        tests/test_stack.py::test_constructor PASSED                                                                                [100%]

So what if we want to create a new test of something else, but define it separately from
the test we just created?
It's a great idea and there are ways to inherit from the test that we have already created.
Let's get into that.

# Fixtures - Easy instantiation for our Push test
You don't want to rewrite instantiations over and over. We have a fantastic feature from pytest called
fixtures that can help us there.
What the fixture can do for you is predefine an instantiation of a class.
You can then inject the instantiation into your test function.
In our case here, it will only save us one line in our future test functions,
but you can imagine how this could be nice in a larger project.

To use fixtures it's a three step process.
1 - import the pytest module into our test file.

        import pytest
        
2 - Create a normal function and decorate it to make it into a fixture.
What we can do after this creation is call stack() and it will return a Stack object to us.

        @pytest.fixture  # makes function to fixture
        def stack():  # just a name
            return Stack()  # Instantiate and return a Stack class object
            
3 - Call the stack() fixture to inject the class object into your function.
We call the fixture by putting it as an argument to our test function.
We also call a method on the object to get a value onto the stack.
And we assert to see that our list len is now 1.

        def test_push(stack):  # Call stack fixture to get class Object
            stack.push(3)  # Call "push" method on the class object. Push a value on to the stack data structure
            assert len(stack) == 1  # check that the len is now 1 as we pushed a value onto it.

        E       AttributeError: 'Stack' object has no attribute 'push'

Naturally, the push method failed. Good TDD there, we wrote the test first and it failed.
Now let's fix the code so that our test passes.
We create a simple static method in our class that will receive an unknown variable.
We will as such just call the argument "item". 
We will then append the item to the internal list we called _storage

        def push(self, item):
            self._storage.append(item)
        
        tests/test_stack.py::test_constructor PASSED                                                                                [ 50%]
        tests/test_stack.py::test_push PASSED

# Pop test and method
Let's continue with the easy instantiation way here. We will inject a new class object for pop tests.

        def test_pop(stack):  # Inject a new instantiation of the stack class object
            stack.push("hello")  # Push a value onto the stack
            stack.push("world")  # Push a second value onto the stack
            assert stack.pop() == "world"  # Test if we can pop world
            assert stack.pop() == "hello"  # Test if we can pop hello
            assert stack.pop() is None  # Test if an additional pop will return None
              
        E       AttributeError: 'Stack' object has no attribute 'pop'

Of course the first test fails, good stuff.
So the thinking is that we will push two strings onto our new stack and then pop the values.
Let's define the static method in our class for this.

        def pop(self):
            try:
                item = self._storage.pop()
            except IndexError:
                item = None
            return item

Okay so luckily there's a built-in function in lists to pop items.
Pop means to take the last item put on the stack off. We store that in
a variable called item that we'll return.
If no values are left in the list when we run the pop method, we will get an 
IndexError. We capture the IndexError and will set the item to return to None.
Why? Because that's what we said in our test :).

        tests/test_stack.py::test_constructor PASSED                                                                                [ 33%]
        tests/test_stack.py::test_push PASSED                                                                                       [ 66%]
        tests/test_stack.py::test_pop PASSED

All tests passed.          

# pytest coverage
How many lines of code did you actually test in your code?
There's a plugin called pytest-cov that can help us with this. 
Install it first with:

        pip3 install pytest-cov

Now we can run it with:

        python -m pytest -v --cov
        
        data_structure/stack.py                                
            13      0   100%

It's quite verbose, but what we're interested in is the stack.py file tests.
The output is telling us that we're testing 13 lines of code, or 100% of the lines 
in the file that has code on them (comments excluded).

           