from data_structure.stack import Stack
import pytest  # so we can use fixtures


@pytest.fixture  # makes function to fixture
def stack():  # just a name
    return Stack()  # Instantiate and return a Stack class object


# Constructing an object from a class done with a constructor.
# This is what we're going to test

# Names of the functions must be prepended with test_ !

# Test for class constructor (instantiation)
def test_constructor():
    stack_object = Stack()  # Instantiate a stack object, manual method.
    assert isinstance(stack_object, Stack)
    assert len(stack_object) == 0


# test for a push onto the data structure stack
def test_push(stack):  # Pass fixture as argument to get class Object
    stack.push(3)  # Call "push" method on the class object. Push a value on to the stack data structure
    assert len(stack) == 1  # check that the len is now 1 as we pushed a value onto it.
    stack.push(5)
    assert len(stack) == 2


# test for popping items off the stack
def test_pop(stack):  # Inject a new instantiation of the stack class object
    stack.push("hello")  # Push a value onto the stack
    stack.push("world")  # Push a second value onto the stack
    assert stack.pop() == "world"  # Test if we can pop world
    assert stack.pop() == "hello"  # Test if we can pop hello
    assert stack.pop() is None  # Test if an additional pop will return None
