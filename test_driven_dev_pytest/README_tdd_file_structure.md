# File/Directory Structure
I believe it's sort of important with a file and directory structure for testing.

## App directory
Our project is about a "stack data structure" and will as such have a folder for the app
which we call data_structure.
The empty __init__.py is basically making the "data_structure" directory into a package.
The stack.py will contain our code for the data structure.

## Test directory
This is where we put our test files.
NOTE! For pytest, all files that we test with must be prefaced with "test_"!
So we name our file test_stack.py

