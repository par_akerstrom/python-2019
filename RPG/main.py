from classes.game import Person, BColours
from classes.magic import Spell
from classes.inventory import Item
import random

print("\n\n")
# Create Black Magic
fire = Spell("Fire", 10, 100, "Black")
thunder = Spell("Thunder", 10, 100, "Black")
blizzard = Spell("Blizzard", 10, 100, "Black")
meteor = Spell("Meteor", 20, 200, "Black")
quake = Spell("Quake", 14, 140, "Black")

# Create White Magic
cure = Spell("Cure", 12, 620, "White")
cura = Spell("Cura", 18, 1500, "White")

# Create Items
potion = Item("Potion", "potion", "Heals 50 HP", 50)
hipotion = Item("Hi-Potion", "potion", "Heals for 100 HP", 100)
superpotion = Item("Super Potion", "potion", "Heals for 1000 HP", 1000)
elixir = Item("Elixir", "elixir", "Fully restores HP/MP of one party member", 9999)
megaelixir = Item("Mega Elixir", "elixir", "Fully restores HP/MP of all party members", 9999)
grenade = Item("Grenade", "attack", "Deals 500 damage", 500)

player_spells = [fire, thunder, meteor, cure, cura]
player_items = [{"item": potion, "quantity": 15},
                {"item": hipotion, "quantity": 5},
                {"item": superpotion, "quantity": 1},
                {"item": elixir, "quantity": 3},
                {"item": megaelixir, "quantity": 1},
                {"item": grenade, "quantity": 3}]

enemy_spells = [meteor, cura]

# Instantiate People
player1 = Person("Valos:", 3260, 100, 300, 34, player_spells, player_items)
player2 = Person("Nick :", 4160, 60, 311, 34, player_spells, player_items)
player3 = Person("Robot:", 3089, 150, 288, 34, player_spells, player_items)

enemy1 = Person("Imp  ", 1250, 130, 9999, 325, enemy_spells, [])
enemy2 = Person("Magus", 18460, 60, 9999, 25, enemy_spells, [])
enemy3 = Person("Imp  ", 1250, 130, 9999, 325, enemy_spells, [])

players = [player1, player2, player3]
enemies = [enemy1, enemy2, enemy3]

running = True

while running:
    print("==========================================")
    print("\n\n")
    print("NAME                     HP                                     MP           ")
    for player in players:
        player.get_stats()

    print("\n")
    print("ENEMY                    HP                                                  ")
    for enemy in enemies:
        enemy.get_enemy_stats()

    for player in players:
        player.choose_action()
        choice = input("    Choose Action: ")
        index = int(choice) - 1

        if index == 0:
            enemy = player.choose_target(enemies)
            dmg = player.generate_damage()
            enemies[enemy].take_damage(dmg)
            print(BColours.OKGREEN + BColours.BOLD +
                  "You attacked " + enemies[enemy].name + " " + BColours.ENDC +
                  "for {} points of damage.".format(dmg))

            if enemies[enemy].get_hp() == 0:
                print(enemies[enemy].name.replace(" ", "") + " has died")
                del enemies[enemy]

        elif index == 1:
            player.choose_magic()
            magic_choice = int(input("    Choose Spell: ")) - 1
            if magic_choice == -1:
                continue
            spell = player.magic[magic_choice]
            current_mp = player.get_mp()
            if spell.cost > current_mp:
                print(BColours.FAIL + "You don't have enough MP" + BColours.ENDC)
                continue
            enemy = player.choose_target(enemies)
            player.reduce_mp(spell.cost)
            magic_dmg = spell.generate_damage()

            if spell.type is "White":
                player.heal(magic_dmg)
                print(BColours.OKGREEN + BColours.BOLD +
                    spell.name + " healed for " + BColours.ENDC +
                    "{} HP.".format(magic_dmg))
            elif spell.type is "Black":
                enemies[enemy].take_damage(magic_dmg)
                print(BColours.OKBLUE + BColours.BOLD +
                    spell.name + " did " + BColours.ENDC +
                    "{} points of damage to {}".format(magic_dmg, enemies[enemy].name))
                if enemies[enemy].get_hp() == 0:
                    print(enemies[enemy].name.replace(" ", "") + " has died")
                    del enemies[enemy]

        elif index == 2:
            player.choose_item()
            item_choice = int(input("    Choose item: ")) - 1
            if item_choice == -1:
                continue
            item = player.items[item_choice]["item"]

            if player.items[item_choice]["quantity"] == 0:
                print(BColours.FAIL + "No {} left.".format(player_items[item_choice]["item"].name))
                continue
            player.items[item_choice]["quantity"] -= 1

            if item.type is "potion":
                player.heal(item.prop)
                print(BColours.OKGREEN + "\n{} heals for {} HP".format(item.name, item.prop))
            elif item.type is "elixir":
                if item.name is "Mega Elixir":
                    for player in players:
                        elixir_hp_heal = player.maxhp - player.hp
                        elixir_mp_heal = player.maxmp - player.mp
                        player.hp = player.maxhp
                        player.mp = player.maxmp
                        print(BColours.OKGREEN + "\n{} heals for {} HP and {} MP"
                              .format(item.name, elixir_hp_heal, elixir_mp_heal))
                else:
                    elixir_hp_heal = player.maxhp - player.hp
                    elixir_mp_heal = player.maxmp - player.mp
                    player.hp = player.maxhp
                    player.mp = player.maxmp
                    print(BColours.OKGREEN + "\n{} heals for {} HP and {} MP"
                          .format(item.name, elixir_hp_heal, elixir_mp_heal))
            elif item.type is "attack":
                enemy = player.choose_target(enemies)
                enemies[enemy].take_damage(item.prop)
                print(BColours.FAIL + "\n{} does {} damage to {}".format(item.name, item.prop, enemies[enemy].name))
                if enemies[enemy].get_hp() == 0:
                    print(enemies[enemy].name.replace(" ", "") + " has died")
                    del enemies[enemy]

    defeated_enemies = 0
    defeated_players = 0

    for enemy in enemies:
        if enemy.get_hp() == 0:
            defeated_enemies += 1

    for player in players:
        if player.get_hp() == 0:
            defeated_players += 1

    if defeated_enemies == 2:
            print(BColours.OKGREEN + "You win!" + BColours.ENDC)
            running = False

    elif defeated_players == 2:
        print(BColours.FAIL + "Your enemies have defeated you. You lose." + BColours.ENDC)
        running = False

    for enemy in enemies:
        enemy_choice = random.randrange(0, 3)
        target = random.randrange(0, 3)

        if enemy_choice == 0:
            enemy_dmg = enemy.generate_damage()
            players[target].take_damage(enemy_dmg)
            print(BColours.FAIL + BColours.BOLD +
                  enemy.name.replace(" ", "") + " attacks " + BColours.ENDC + players[target].name.replace(":", "") + " for {} points of damage. {} HP: {}"
                  .format(enemy_dmg, players[target].name, players[target].get_hp()))
        elif enemy_choice == 1:
            spell, magic_dmg = enemy.choose_enemy_spell()
            if spell.type is "White":
                enemy.heal(magic_dmg)
                print(BColours.OKGREEN + BColours.BOLD +
                      enemy.name + "cast " + spell.name + " which healed for " + BColours.ENDC +
                      "{} HP.".format(magic_dmg))
            elif spell.type is "Black":
                players[target].take_damage(magic_dmg)
                print(BColours.FAIL + BColours.BOLD +
                      enemy.name.replace(" ", "") + " cast " + spell.name + " on " + BColours.ENDC + players[target].name.replace(":", "") + " for {} points of damage. {} HP: {}"
                      .format(magic_dmg, players[target].name, players[target].get_hp()))
            enemy.reduce_mp(spell.cost)

        elif enemy_choice == 2:
            print("enemy chose item")
        if players[target].get_hp() == 0:
            print(players[target].name.replace(":", "").replace(" ", "") + " has died")
            del players[target]