import requests
import simplejson as json

url = "https://www.googleapis.com/urlshortener/v1/url"

payload = {"LongUrl": "http://example.com"}
headers = {"Content-Type": "application/json"}
r = requests.post(url, json=payload, headers=headers)

# Answer comes back as a json string. We load that with json load string.
# We can then parse out the necessary information
print("Your error code is: " + str(json.loads(r.text)["error"]["code"]))
print("reason for deny from google is: " + str(json.loads(r.text)["error"]["errors"][0]["reason"]))

# Bonus content, headers. It gives us useful metadata.
print(r.headers)