from PIL import Image
import requests
from io import BytesIO

r = requests.get("http://pngimg.com/uploads/pizza/pizza_PNG7143.png")

print("status code: ", r.status_code)

# Image class and the method open.
# Pass r.content (binary data) into BytesIO class
image = Image.open(BytesIO(r.content))

# print information about our image
print(image.size, str.lower(image.format), str.lower(image.mode))

# Set path for our image destination
path = ".\image1." + str.lower(image.format)

# use image.save method with path and file format to save image to disk
try:
    image.save(path, image.format)
except IOError:
    print("Couldn't save image")