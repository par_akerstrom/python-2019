from bs4 import BeautifulSoup
import requests
from PIL import Image
from io import BytesIO
import os


def StartSearch():
    search = input("Search for: ")
    # Set GET attribute "q" to search input string
    params = {"q": search}
    # Set directory to search string, without spaces and all lower case
    dir_name = "./bing_images/" + search.replace(" ", "_").lower()

    if not os.path.isdir(dir_name):
        os.makedirs(dir_name)
    # Search bing images
    r = requests.get("http://www.bing.com/images/search", params=params)

    # Create a soup
    soup = BeautifulSoup(r.text, "html.parser")
    # Find all links with a thumbnail
    links = soup.findAll("a", {"class": "thumb"})

    i = 1
    # loop through links for image objects
    for item in links[0:5]:
        img_obj = requests.get(item.attrs["href"])
        # Splits at the last (-1) slash, saves into title variable.
        # used to save the image
        title = item.attrs["href"].split("/")[-1]
        try:
            img = Image.open(BytesIO(img_obj.content))
        except Exception as e:
            print("Couldn't open image due to: {}".format(e))
        try:
            img.save(dir_name + "/" + search + title, img.format)
            print("Saved image number {}; {} {} from {}".format(
                i, search, title, item.attrs["href"]))
        except Exception as e:
            print("Couldn't save image, due to: {}".format(e))
        i += 1

    StartSearch()


StartSearch()
