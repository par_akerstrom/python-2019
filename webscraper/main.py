from bs4 import BeautifulSoup
import requests
from io import BytesIO

search = input("Input search term: ")
# Sets the GET variable of q to the contents of search
params = {"q": search}
r = requests.get("https://www.bing.com/", params=params)

# make the reply into a beautiful soup
soup = BeautifulSoup(r.text, "html.parser")

# We will use methods (find/findAll) to find/parse sections
# of the reply content
# The lecturer graphically found what we want in a browser with an app
# that displays the content

# We want to find <ol id=b_results>
# This is an ordered list of the bing results
# These are the bind search results

results = soup.find("ol", {"id": "b_results"})

# Stepping further into the results, we are now looking for "<li class=b_algo>"
# These are the list items with the class set to b_algo.. These are the links
# to the results on bing's page basically.

links = results.findAll("li", {"class": "b_algo"})

# So now we get into even more html specific digging in the soup.
# the html code looks like <a href="link to webpage".....>
# So we will step through all links and use the find method to find all
# "a" text. Also using a special variable property to get the href statements.
for item in links:
    # grabbing the text, this is the html text displayed in the link
    item_text = item.find("a").text
    # grabbing the href attribute (the URL)
    item_href = item.find("a").attrs["href"]

# If text and URL exists, we print them out, text first, link after
    if item.text and item_href:
        print(item_text)
        print(item_href)

# I spent a lot of time above covering find_all() and find().
# The Beautiful Soup API defines ten other methods for searching the tree,
# but don’t be afraid.
# Five of these methods are basically the same as find_all(),
# and the other five are basically the same as find().
# The only differences are in what parts of the tree they search.

# Remember that find_all() and find() work their way down the tree,
# looking at tag’s descendants.
# These methods do the opposite: they work their way up the tree,
# looking at a tag’s (or a string’s) parents.
# Let’s try them out, starting from a string buried deep in the “three
# daughters” document:
        print("Parent of a element: ", item.find("a").parent)
        # step up two parents, find the p element, print its text
        try:
            print("Summary of bing result: ",
                  item.find("a").parent.parent.find("p").text)
        except BaseException:
            print("oops we couldn't find the parent's parent")
        # get the children of the links, should be a <h2> and a <div>
        children = item.children
        for child in children:
            print("html child to bing link: ", child)

        # children = item.find("h2")
        # print("Sibling of this child is: ", children.next_sibling)
