import web

urls = (
    '/', 'home'
)

# Pointing to our html files directory.
# Base file is Mainlayour. MainLayout loads home.html into it.
render = web.template.render("Views/Templates", base="MainLayout")

app = web.application(urls, globals())

# Classes/routes
class home:
    def GET(self):
        # Return home.html from Views/Templates/
        return render.home()


if __name__ == "__main__":
    app.run()