from time import perf_counter
# 0, 1, 1, 2, 3, 5, 8, 13, 21...

# Crappy way with recursion that is O(n**2) I believe due to recursion calls.
def fib1(n: int) -> int:
    if n < 2:  # Base case
        return n
    return fib1(n - 1) + fib1(n - 2)


# Memoisation is a technique where you store the computations for reuse
# Believe the typing import is just for type hints
from typing import Dict
# Declaration type hint style
memo: Dict[int, int] = {0: 0, 1: 1}

# Trace back with recursion to base cases 0 and 1.
# All new base cases are stored in the dictionary
def fib2(n: int) -> int:
    if n not in memo:
        print("n wasn't found in our dict, doing recursion again")
        memo[n] = fib2(n - 1) + fib2(n - 2)
    print("n was now found, returning dict[n] value")
    return memo[n]


# We can use fancy automagic memoisation with functools!
# Our old crappy example will use
from functools import lru_cache

@lru_cache(maxsize=None)
def fib3(n: int) -> int:  # same definition as fib2()
    if n < 2:  # base case
        return n
    return fib3(n - 2) + fib3(n - 1)  # recursive case

def fib4(n: int) -> int:
    if n == 0: return n  # special case
    last: int = 0  # initially set to fib(0)
    next: int = 1  # initially set to fib(1)
    for _ in range(1, n):
        last, next = next, last + next
    return next


# OK so what about generating a range of fibonacci numbers?
# We use a generator
from typing import Generator

def fib5(n: int) -> Generator[int, None, None]:
    yield 0  # special case
    if n > 0: yield 1  # special case
    last: int = 0  # initially set to fib(0)
    next: int = 1  # initially set to fib(1)
    for _ in range(1, n):
        last, next = next, last + next
        yield next  # main generation step


if __name__ == '__main__':
    start = perf_counter()
    print(fib3(80))
    elapsed = perf_counter() - start
    print(f'lru_cache took: {elapsed:0.6f}')

    start = perf_counter()
    print(fib4(80))
    elapsed = perf_counter() - start
    print(f'n-1 iterations took: {elapsed:0.6f}')

    print(list(fib5(15)))
