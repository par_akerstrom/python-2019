from classes.genericdb import RegisterModel

my_pymongo_conn = RegisterModel()

# Create a variable that we will insert in the db
user1 = {
    "username": "par",
    "password": "suchasecret!",
    "favourite_number": 999,
    "hobbies": ["gamez", "snakes", "burgers"]}

uid = my_pymongo_conn.insert_user(user1)
print("Your newly created user's id is: ", uid)


result = my_pymongo_conn.check_password(uid, user1["password"])
print("Did the passwords match?", result)