from pymongo import MongoClient
import pymongo
# Import datetime to timestamp inserts into our collections
import datetime

current_date = datetime.datetime.now()
old_date = datetime.datetime(2018, 2, 5) # Feb 5 2018

# Instantiate client to the db
myClient = MongoClient()
print("Our client is: ", myClient)
# We can explicitly connect to localhost over mongodb protocol:
# client = MongoClient('mongodb://localhost:27017/')

# Create db by calling method in the myClient class
# mydb is the name we've chosen for our db.
db = myClient.mydb
print("Our db is: ", db)
# We can also use this syntax if we want dashes in our name
# db = myClient["my-db"]

# Table in sql = collection in nosql
# Create our first collection - users
# variable = db.<collection_name>
users = db.users
print("Our collection is: ", users)

# Create a variable that we will insert in the db
user1 = {
    "username": "par",
    "password": "myverysecurepassword",
    "favourite_number": 445,
    "hobbies": ["python", "games", "pizza"]}

# insert one document (user1) into our collection users. Return inserted id.
if users.count_documents({}) < 50:
    user_id1 = users.insert_one(user1).inserted_id
    print(user_id1)

# Create a variable that we will insert in the db
user2 = {
    "username": "someone",
    "password": "anotherverysecurepassword",
    "favourite_number": 445,
    "hobbies": ["java", "lyfe", "pasta"]}

# insert one document (user1) into our collection users. Return inserted id.
if users.count_documents({}) < 50:
    user_id2 = users.insert_one(user2).inserted_id
    print(user_id2)

# We can also conveniently bulk insert many documents in one go.
# Use a list as input and use the insert_many method
# to insert in our collection (table).

multiple_users = [
    {"username": "ben", "password": "supersecret"},
    {"username": "holly", "password": "supersecret"}
]

if users.count_documents({}) < 50:
    bulk_insert_response = users.insert_many(multiple_users)
    print("IDs for bulk inserted users: ",
      bulk_insert_response.inserted_ids)


multiple_users_with_dates = [
    {"username": "howard", "date": current_date},
    {"username": "molly", "date": old_date}
]

if users.count_documents({}) < 60:
    bulk_insert_response = users.insert_many(
        multiple_users_with_dates)
    print("IDs for bulk inserted users: ",
          bulk_insert_response.inserted_ids)

### Searching our database ###
# Count of documents is done per collection.
# Command is collection.count_documents({'Filter'})
print("Total amount of user entries:", users.count_documents({}))
print("Total amount of user entries named \"ben\""
      " and password \"supersecret\"",
      users.count_documents(
          {"username": "ben", "password": "supersecret"}))

# $keywords can be used for searching, such as $gt, $lt
print("Total amount of user entries with date"
      "greater than or equal to old_date",
      users.count_documents(
          {"date": {"$gte": old_date}}))

# $exists to find specific fields
print("Total amount of user entries without field"
      " \"date\" set",
      users.count_documents(
          {"date": {"$exists": False}}))

# $exists to find specific fields
print("Total amount of user entries with username"
      " field not equal to \"ben\"",
      users.count_documents(
          {"username": {"$ne": "ben"}}))

# Find specific entries with find_one
found_in_collection = users.find_one({"username": "ben"})
print("info about a user with username ben: ", found_in_collection)


### Updating our nosql database ###
# Using previous search's found user. Updating field called "updated".
users.update_one(
    {'_id': found_in_collection['_id']},
    {"$set": {"updated": "I was modified"}}, upsert=False)
found_in_collection = users.find_one({"updated": "I was modified"})
print(found_in_collection)


### Indexes
# Normal find searches goes through EVERY field in every document.
# Instead, we can create indexes for popular queries.

#
print(users.find_one({"username": "ben"}))

# Create index with a list of tuples
users.create_index([("username", pymongo.ASCENDING)])

# From now on, our search may be using the index.
# didn't really test this functionality but we can see that an
# index in robomongo has been created at least :).
print(users.find_one({"username": "ben"}))

