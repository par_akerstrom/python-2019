from pymongo import MongoClient
import datetime
import bcrypt

current_date = datetime.datetime.now()
old_date = datetime.datetime(2018, 2, 5) # Feb 5 2018


class RegisterModel:
    def __init__(self):
        # Note that we can explicitly connect to localhost over mongodb protocol:
        # client = MongoClient('mongodb://localhost:27017/')
        # Or implicitly use localhost by leaving it out
        self.client = MongoClient()
        self.db = self.client.mydb # mydb refers to the db in pymongo
        self.Users = self.db.users # Create collection (table) users in mydb

    def insert_user(self, data):
        hashed = bcrypt.hashpw(data["password"].encode(), bcrypt.gensalt())
        id = self.Users.insert_one({
            "username": data["username"], "favourite_number": data["favourite_number"],
        "password": hashed, "hobbies": data["hobbies"]}).inserted_id
        return id

    def check_password(self, userid, password_to_check):
        found_in_collection = self.Users.find_one({"_id": userid})
        hashed_pw = found_in_collection["password"]
        if bcrypt.checkpw(password_to_check.encode(), hashed_pw):
            return "Passwords do match"
        else:
            return "Passwords do not match"
