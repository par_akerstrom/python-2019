## main.py raw PyQt
Creates a window. I don't know much or any about the config.
It basically just creates an empty window.

## pyqml.py
A markup language approach to pyqt, like css/html/javascript style.
The python component of this is very small, just a bunch of import statements and some declaration.
The rest is set in pyqml.qml.

We're using Qt Quick Controls to create our GUI.
References for qml is at: https://doc.qt.io/qt-5/qtquick-controls2-qmlmodule.html

        ! Note that to show console output in PyCharm you need to enable:
        "emulate terminal in output console" in Run -> Edit Configurations
       