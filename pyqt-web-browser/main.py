import sys
import os
# import json

from PyQt5.QtWidgets import (QApplication, QWidget,
                             QVBoxLayout, QHBoxLayout,
                             QPushButton, QLabel, QLineEdit,
                             QTabBar, QFrame, QStackedLayout,
                             QShortcut, QKeySequenceEdit)

from PyQt5.QtGui import QIcon, QWindow, QImage, QKeySequence
from PyQt5.QtCore import *
from PyQt5.QtWebEngineWidgets import *

class AddressBar(QLineEdit):
    def __init__(self):
        super().__init__()

    def mousePressEvent(self, e):
        self.selectAll()

class App(QFrame):  # Inherit from QFrame class
    def __init__(self):
        super().__init__()  # Initialise parent superclass
        self.setWindowTitle("Par's QtWebEngine Web Browser")
        self.setBaseSize(1366, 768)
        self.setMinimumSize(1366, 768)
        self.CreateApp()  # Instantiate function CreateApp

    def CreateApp(self):
        self.layout = QVBoxLayout()  # We're using a vertical box layout
        self.layout.setSpacing(0)  # Remove spacing between stuff
        self.layout.setContentsMargins(0, 0, 0, 0)  # remove side spacing for aesthetics

        # Create Tabs at the top
        self.tabbar = QTabBar(movable=True, tabsClosable=True)  # Create tab bar

        self.tabbar.tabCloseRequested.connect(self.CloseTab)  # Signal from gui to close tab
        # Send this signal to our custom function CloseTab. It sends a parameter with the signal
        # (the tab reference) that we can use in the function.
        self.tabbar.tabBarClicked.connect(self.SwitchTab)  # Another signal, so remove parenthesis and add .connect

        self.tabbar.setCurrentIndex(0)
        self.tabbar.setDrawBase(False)

        self.shortcutNewTab = QShortcut(QKeySequence("Ctrl+T"), self)
        self.shortcutNewTab.activated.connect(lambda: self.AddTab("https://google.com.au"))

        self.shortcutReload = QShortcut(QKeySequence("Ctrl+R"), self)
        self.shortcutReload.activated.connect(self.Reload)

        self.shortcutCloseTab = QShortcut(QKeySequence("Ctrl+W"), self)
        self.shortcutCloseTab.activated.connect(self.CloseTab)

        # Keep track of tabs
        self.tabCount = 0
        self.tabs = []  # contains tab widgets objects

        # Create AddressBar/Toolbar in the middle
        self.Toolbar = QWidget()
        self.Toolbar.setObjectName("Toolbar")
        self.ToolbarLayout = QHBoxLayout()
        self.addressbar = AddressBar()


        # Set Toolbar Buttons
        self.AddTabButton = QPushButton("New Tab")
        self.BackButton = QPushButton("<")
        self.ForwardButton = QPushButton(">")
        self.ReloadButton = QPushButton("\u21ba")
        self.AboutButton = QPushButton("About")

        # Connect addressbar + button signals
        self.AddTabButton.clicked.connect(lambda: self.AddTab("https://google.com.au"))
        self.ReloadButton.clicked.connect(self.Reload)
        self.ForwardButton.clicked.connect(self.GoForward)
        self.BackButton.clicked.connect(self.GoBack)
        self.AboutButton.clicked.connect(self.About)
        self.addressbar.returnPressed.connect(self.BrowseTo)  # Signal when enter is pressed in the addressbar
        # Above will call the BrowseTo function with the data currently in the addressbar


        # Build toolbar
        self.Toolbar.setLayout(self.ToolbarLayout)
        self.ToolbarLayout.addWidget(self.BackButton)
        self.ToolbarLayout.addWidget(self.ForwardButton)
        self.ToolbarLayout.addWidget(self.ReloadButton)
        self.ToolbarLayout.addWidget(self.addressbar)
        self.ToolbarLayout.addWidget(self.AddTabButton)
        self.ToolbarLayout.addWidget(self.AboutButton)


        # Set main view at the bottom
        self.container = QWidget()
        self.container.layout = QStackedLayout()  # Set a stacked layout for our container
        self.container.setLayout(self.container.layout)

        # Construct main view from top level elements
        self.layout.addWidget(self.tabbar)
        self.layout.addWidget(self.Toolbar)
        self.layout.addWidget(self.container)
        self.setLayout(self.layout)

        self.AddTab("https://google.com.au")
        self.show()

    def CloseTab(self, tab_reference):
        self.tabbar.removeTab(tab_reference)

    def AddTab(self, url):
        i = self.tabCount

        # Set self.tabs<#> == QWidget
        self.tabs.append(QWidget())  # Create QWidget
        self.tabs[i].layout = QVBoxLayout()  # Modify widget we just appended above
        self.tabs[i].layout.setContentsMargins(0, 0, 0, 0,)

        self.tabs[i].setObjectName("tab" + str(i))  # Modify widget again we just appended

        # Open webview
        self.tabs[i].content = QWebEngineView()  # Set current tab's content to a view
        self.tabs[i].content.load(QUrl.fromUserInput(url))  # Load view into QWebEngineView.
        # Above converts text (google.com) into information that Qt can use to do stuff.

        # Send signal whenever website title changes
        self.tabs[i].content.titleChanged.connect(lambda: self.SetTabContent(i, "title"))
        self.tabs[i].content.iconChanged.connect(lambda: self.SetTabContent(i, "icon"))
        self.tabs[i].content.urlChanged.connect(lambda: self.SetTabContent(i, "url"))

        # add webview to tabs layout
        self.tabs[i].layout.addWidget(self.tabs[i].content)

        # Set top level tab from [] to layout
        self.tabs[i].setLayout(self.tabs[i].layout)

        # Add tab to top level stackedwidget
        self.container.layout.addWidget(self.tabs[i])
        self.container.layout.setCurrentWidget(self.tabs[i])

        # Create tab on tabbar, representing this tab
        # Set tabdata to tab<#> so it knows what self.tabs[#] it needs to control
        self.tabbar.addTab("New Tab")
        self.tabbar.setTabData(i, {"object": "tab" + str(i), "initial": i})
        # self.tabbar.setTabData(i, "tab" + str(i))  # To control the tab name
        self.tabbar.setCurrentIndex(i)  # Set index for the tab?

        self.tabCount += 1

    def SwitchTab(self, i):  # Automatically receives signal of which tab that was clicked, we call that index i
        tab_data = self.tabbar.tabData(i)["object"]  # Get tabdata to be able to point to the right tab
        # This preserves the data throughout the runtime of a tab. The index data persists
        # even if you move tabs around!
        print("tab:", tab_data)

        if self.tabbar.tabData(i):
            tab_content = self.findChild(QWidget, tab_data)
            self.container.layout.setCurrentWidget(tab_content)
            new_url = tab_content.content.url().toString()
            self.addressbar.setText(new_url)

    def BrowseTo(self):
        text = self.addressbar.text()
        print(text)

        i = self.tabbar.currentIndex()  # index of the current tab
        tab = self.tabbar.tabData(i)["object"]  # Get tab name
        web_view = self.findChild(QWidget, tab).content  # Get tab view associated with the name

        if "http" not in text:
            if "." not in text:
                url = "https://www.google.com.au/search?q=" + text
            else:
                url = "http://" + text
        else:
            url = text

        web_view.load(QUrl.fromUserInput(url))

    def GoBack(self):
        activeIndex = self.tabbar.currentIndex()
        tab_name = self.tabbar.tabData(activeIndex)["object"]
        tab_content = self.findChild(QWidget, tab_name).content

        tab_content.back()

    def GoForward(self):
        activeIndex = self.tabbar.currentIndex()
        tab_name = self.tabbar.tabData(activeIndex)["object"]
        tab_content = self.findChild(QWidget, tab_name).content

        tab_content.forward()

    def Reload(self):
        activeIndex = self.tabbar.currentIndex()
        tab_name = self.tabbar.tabData(activeIndex)["object"]
        tab_content = self.findChild(QWidget, tab_name).content

        tab_content.reload()

    def About(self):
        self.AddTab("https://github.com/paraker")

    def SetTabContent(self, i, type):
        """
            self.tabs[i].objectName = tab1
            self.tabbar.tabData[i]["object"] = tab1
        """
        tab_name = self.tabs[i].objectName()
        count = 0
        running = True

        current_tab = self.tabbar.tabData(self.tabbar.currentIndex())["object"]

        if current_tab == tab_name and type == "url":
            new_url = self.findChild(QWidget, tab_name).content.url().toString()
            self.addressbar.setText(new_url)
            return False

        while running:
            tab_data_name = self.tabbar.tabData(count)

            if count >= 99:
                running = False

            if tab_name == tab_data_name["object"]:  # If true, we have the right tab
                if type == "title":
                    new_title = self.findChild(QWidget, tab_name).content.title()  # Fetch title from the tab's content
                    self.tabbar.setTabText(count, new_title)
                elif type == "icon":
                    newIcon = self.findChild(QWidget, tab_name).content.icon()
                    self.tabbar.setTabIcon(count, newIcon)
                running = False
            else:
                count += 1


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = App()

    with open("style.css", "r") as style:
        app.setStyleSheet(style.read())  # Apply css to our app

    sys.exit(app.exec_())
