## Package installation
Another, faster approach for PyCharm to understand PyQt5 is this:
Create your new pipenv environment that is empty to start with.
Directly use this:
            
            File -> Settings -> Project -> Project Interpreter -> +
            
To install the PyQt5, PyQt5-sip, sip packages.
Then, the syntax will be recognised straight away by PyCharm. Neat.

## Stacked layout
New layout type used in this project. I believe this is combining layouts.

## QtWebEngine
This is a new package used for this project.

## Signals
There are a couple of signals we're using.
PyCharm thinks these are methods, but they're not used like that.
Examples are:

                self.tabbar.tabCloseRequested.connect(self.CloseTab)  # Signal from gui to close tab
                self.tabbar.tabBarClicked.connect(self.SwitchTab)  # Another signal, so remove parenthesis and add .connect

So PyCharm will add parenthesis after tabCloseRequested() and tabBarClicked().
But what we need to do is remove the parenthesis and add .connect instead.

Additionally, notice how PyCharm wants to add parenthesis to the function call
self.CloseTab(). This again should be deleted! It's not how it works!
It can only be used if we use a lambda function to pass in arguments.
Like this:

                   self.tabs[i].content.titleChanged.connect(lambda: self.SetTabText(i))
