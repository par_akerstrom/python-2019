import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class Application(QWidget):
    def __init__(self):
        super().__init__()  # Instantiates the superclass QWidget
        self.setWindowTitle("Calculator")
        self.CreateApp()  # Call our application instantiation method.
        # Not sure why we are breaking that out all the time...

    def CreateApp(self):
        """
        Grid application
        """

        grid = QGridLayout()

        button1 = QPushButton("One")
        button2 = QPushButton("Two")
        button3 = QPushButton("Three")
        button4 = QPushButton("My last button")

        # Put button in our grid
        grid.addWidget(button1, 0, 0, 1, 1)  # row, column, rows to occupy, columns to occupy
        grid.addWidget(button2, 0, 1, 1, 1)
        grid.addWidget(button3, 0, 2, 1, 1)
        grid.addWidget(button4, 1, 0, 1, 2)
        self.setLayout(grid)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Application()
    sys.exit(app.exec_())
