import sys
from PyQt5.QtWidgets import *
import math


class Button:
    def __init__(self, text, results):
        self.b = QPushButton(str(text))
        self.text = text
        self.results = results
        self.b.clicked.connect(lambda: self.handleInput(self.text))  # connect button to event handler
        # We need lambda functions to pass in an argument in this case to get the
        # event handler working with a variable.
        # Note that each individual class instantiation of buttons have their own
        # link to the handleInput event handler!
        # So when a button is cliked, it will run the handleInput function.
        # The handleInput function will receive the self.text for that
        # button instantiation.

    def handleInput(self, v):
        if v == "=":
            res = eval(self.results.text())  # Use of python excellent eval function
            self.results.setText(str(res))  # Set the results text to eval output
        elif v == "AC":
            self.results.setText("")
        elif v == "√":
            value = float(self.results.text())
            self.results.setText(str(math.sqrt(value)))
        elif v == "DEL":
            current_value = self.results.text()
            self.results.setText(current_value[:-1])
        else:
            current_value = self.results.text()
            new_value = current_value + str(v)
            self.results.setText(new_value)

class Application(QWidget):
    def __init__(self):
        super().__init__()  # Instantiates the superclass QWidget
        self.setWindowTitle("Calculator")
        self.CreateApp()  # Call our application instantiation method.
        # Not sure why we are breaking that out all the time...

    def CreateApp(self):
        """
        Grid application
        """

        grid = QGridLayout()  # Creates our grid layout
        results = QLineEdit()  # Creates the text field

        # define list of buttons
        buttons = ["AC", "√", "DEL", "/",
                   7, 8, 9, "*",
                   4, 5, 6, "-",
                   1, 2, 3, "+",
                   0, ".", "="]

        grid.addWidget(results, 0, 0, 1, 4)  # Add our results QLineEdit at the top of the grid

        row = 1  # Row 0 is already occupied by the results line.
        col = 0
        # Loop through our list to place the button in the grid
        for button in buttons:
            # If there are more than three columns, move to next row.
            if col > 3:
                col = 0
                row += 1

            button_object = Button(button, results)  # instantiate Button class
            # This is necessary to get event handlers for each individual button.
            # So we're basically sending the button in the for loop and the QLineEdit object
            # into the class instantiation

            # button_object.b is the QPushButton from class Button
            # This is what we're adding as a widget
            if button == 0: # We're making the "0" button larger just for aesthetics
                grid.addWidget(button_object.b, row, col, 1, 2)
                col += 1
            grid.addWidget(button_object.b, row, col, 1, 1)
            # Count up a column to move to the next place in grid.
            col += 1

        self.setLayout(grid)
        self.show()  # make it visible on the screen


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Application()
    sys.exit(app.exec_())
