## Issues with PyCharm and PyQT5 package
If links to modules are unresolved in PyCharm.
Go to File -> Settings -> Project Interpreter.
Uninstall PyQt5.
Re-install PyQt5 using the graphical interface in PyCharm.
It will mumble about updating skeletons in the background.
Then the modules will resolve.