## PyAudio installation and speech_recognition
Not entirely sure what actually made speech recognition work.
But these are pretty much all the commands I ran when I started working.

		sudo apt install python-pyaudio python3-pyaudio
		sudo apt install python3-dev
		sudo apt install build-essential
		sudo apt install libpq-dev python-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev
		pip3 install wheel
		pip3 install SpeechRecognition

Test the module with:
		
		python3 -m speech_recognition


## Instructions at RP
https://realpython.com/python-speech-recognition/#picking-a-python-speech-recognition-package