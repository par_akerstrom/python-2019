from commands import Commander
import speech_recognition as sr


running = True
commander_object = Commander()  # Instantiate Commander class object
recogniser_object = sr.Recognizer()  # Instantiate recogniser class object
mic = sr.Microphone()  # Instantiate microphone class object
recogniser_object.pause_threshold = 1.0

def get_user_input():
    with mic as source:
        print("Say something")
        try:
            audio = recogniser_object.listen(source, timeout=2.0)
            voice_input = recogniser_object.recognize_google(audio)
        except:
            print("couldn't hear you")
            voice_input = ""
    

    if voice_input in ["quit", "exit", "stop", "goodbye"]:
    	global running
    	running = False
    	commander_object.respond("Goodbye")
    	exit()
    	
    print(f"Your command: {voice_input}")
    commander_object.discover(voice_input)


while running == True:
	get_user_input()
