from google_speech import Speech
import os

language = "en"


class Commander:
	def __init__(self):
		self.confirm = ["yes", "affirmative", "si", "sure","do it", "yeah", "confirm"]
		self.cancel = ["negative", "no", "negative soldier", "don't", "wait", "cancel"]
		self.name = ""

	def discover(self, text):
		print(text)
		if "hello" in text:
			self.respond("hello there")
		elif "what" in text and "your name" in text:
			self.respond("My name is robot")
		elif "my name is" in text:
			name = text.split(" ", 3)[-1]
			self.store_name(name)
		elif "what" in text and "my name" in text:
			if self.get_name():
				self.respond(self.get_name())
			else:
				self.respond("You haven't told me your name yet")
		elif "open" in text or "launch" in text:
			app = text.split(" ", 1)[-1] # split at the first space, then grab whatever is in the last of the list
			# For example "Launch mozilla firefox" would become "mozilla firefox"
			self.respond("Sure, I'm trying to open" + app + " for you")
			os.system(app)
		else:
			self.respond("I don't know how to help with that yet")


	def respond(self, response):
		speech = Speech(response, language)
		sox_effects = ("speed", "1")
		try:
			speech.play()
		except:
			print("whatever")

	def store_name(self, name):
		self.name = name
		speech = Speech(f"Pleasure to meet you, {name}", language)
		speech.play()

	def get_name(self):
		return self.name