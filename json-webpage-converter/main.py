import requests
import json
import certifi
import pprint
 
class Website:
        def __init__(self, url):
                self.url = url
 
        def get_json_manual_load(self):
                json_formatted_page = requests.get(self.url, verify=False)  # Fetches json formatted webpage with requests
                # Use python json library to deserialise json to python type.
                # Conversion table is object = dict, array = list, string = str etc...
                # This effectively does exactly the same as the direct load
                python_formatted_page = json.loads(json_formatted_page.text)
                return python_formatted_page
 
        def get_json_direct_load(self):
                # Throw the requests object at json() to get an automated deserialisation
                # Conversion table is object = dict, array = list, string = str etc...
                python_formatted_page = requests.get(self.url, verify=False).json()
                return python_formatted_page
 
        def compare_python_objects(self):
                # Method to prove that the manual and direct load deserialisation is the same
                if self.get_json_manual_load() == self.get_json_direct_load():
                        return True
                else:
                        return False
 
        def get_json_page_type(self):
                # returns the type of python object that it has deserialised the json format to
                python_deserialised_page = requests.get(self.url, verify=False).json()
                return type(python_deserialised_page)
 
        def print_json_page_pretty(self):
                # Prints a page with indentation, for readability
                page = self.get_json_direct_load()
                print(json.dumps(page, indent=4))
 
 
class StatusCake(Website):
        """
        Parent superclass is Website.
        This Class has access to all of the superclass' functions
        """
        def get_statuscake_ips(self):
                page = self.get_json_direct_load()
                ip_list = []
                for number in page:
                        ip_list.append(page[number]['ip'])
                return ip_list
 
        def print_statuscake_cisco_config(self):
                newest_list = self.get_statuscake_ips()
                print("h2. asa config\n" +
                        "object-group network statuscake_whitelist")
                for ip in newest_list:
                        print("network-object host {}".format(ip))
 
 
def main():
        url = "https://app.statuscake.com/Workfloor/Locations.php?format=json"
        url2 = "https://jsonplaceholder.typicode.com/todos"
        # headers = {'Content-Type': 'application/json', }
        statuscake_object = StatusCake(url)
        jsonplaceholder_object = Website(url2)
        statuscake_object.print_statuscake_cisco_config()
 
main()
