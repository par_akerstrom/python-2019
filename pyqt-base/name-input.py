import sys # To run our app
from PyQt5.QtWidgets import (QWidget, QApplication,
                             QHBoxLayout, QVBoxLayout,
                             QPushButton, QLabel,
                             QLineEdit)


# Layouts are used for things to stay within borders (our layout)

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()  # initialising parent (Qwidget)
        self.init_ui()
        self.counter = 0


    def init_ui(self):
        """
        Runs all our code
        :return:
        """

        self.text_label = QLabel("There has been no name entered, so I can't do anything yet")
        self.label = QLabel("Name: ")
        self.name_input = QLineEdit()
        self.button = QPushButton("Clicked: 0")

        horizontal = QHBoxLayout()
        horizontal.addWidget(self.label)
        horizontal.addWidget(self.name_input)

        self.button.setText("Set Name")
        # Creates a connection from the button click action.
        # The button click sends us to the function self.alter_name
        self.button.clicked.connect(self.alter_name)  # Note here too that () is used for the function call.

        vertical = QVBoxLayout()
        vertical.addWidget(self.text_label)
        vertical.addLayout(horizontal)
        vertical.addWidget(self.button)

        self.setLayout(vertical)

        self.setWindowTitle("My Application")
        self.show()

    def alter_name(self):
        inputted_text = self.name_input.text()  # Grabs text from name_input field
        our_string = "you've entered: {}".format(inputted_text)
        self.text_label.setText(our_string)  # Set text label to our name_input field
        self.setWindowTitle(inputted_text)  # Set window title


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
