## QPushButton
https://wiki.qt.io/How_to_Use_QPushButton

The underlying functions of PyQT is written in C++. This becomes apparent when
we are looking at the function specifics such as QPushButton on the Qt wiki.

Signals from QPushButton are as per the wiki:
* void clicked ( bool checked = false )
* void pressed ()
* void released ()
* void toggled ( bool checked )

These can be used in PyQt with python syntax as class methods.
Such as ok_button.pressed.connect()