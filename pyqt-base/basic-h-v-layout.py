import sys # To run our app
from PyQt5.QtWidgets import (QWidget, QApplication,
                             QHBoxLayout, QVBoxLayout,
                             QPushButton, QLabel)


# Layouts are used for things to stay within borders (our layout)

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()  # initialising parent (Qwidget)
        self.init_ui()

    def init_ui(self):
        """
        Runs all our code
        :return:
        """
        label = QLabel("Hi there, I'm a label")
        ok_button = QPushButton("OK")
        cancel_button = QPushButton("Cancel")

        horizontal = QHBoxLayout()  # Creates horizonal layout
        horizontal.addStretch()  # Makes button not stretch out
        horizontal.addWidget(ok_button)  # Add button to horizontal layout
        horizontal.addWidget(cancel_button)  # Add button to horizontal layout

        vertical = QVBoxLayout()  # Add vertical layout
        vertical.addStretch()  # resized when necessary
        vertical.addWidget(label)  # Add label to vertical layout
        vertical.addLayout(horizontal)  # Add horizontal to the vertical layout

        self.setLayout(vertical)  # Adds both layouts to the window

        self.setWindowTitle("Horizontal Layout")
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
