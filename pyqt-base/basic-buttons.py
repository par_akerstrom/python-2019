import sys # To run our app
from PyQt5.QtWidgets import (QWidget, QApplication,
                             QHBoxLayout, QVBoxLayout,
                             QPushButton, QLabel,
                             QLineEdit)


# Layouts are used for things to stay within borders (our layout)

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()  # initialising parent (Qwidget)
        self.init_ui()
        self.counter = 0

    def init_ui(self):
        """
        Runs all our code
        :return:
        """
        label = QLabel("Name: ")
        name_input = QLineEdit()
        self.button = QPushButton("Clicked: 0")
        self.button.pressed.connect(self.pressed_button)  # pass a function as a slot
        self.button.released.connect(self.released_button)
        # Notice how we don't use () at the end of the function call to self.released_button.
        # To pass variables, we need a different syntax in PyQt. Hopefully more on that later.

        horizontal = QHBoxLayout()
        horizontal.addWidget(label)
        horizontal.addWidget(name_input)

        vertical = QVBoxLayout()
        vertical.addLayout(horizontal)
        vertical.addWidget(self.button)

        self.setLayout(vertical)

        self.setWindowTitle("Horizontal Layout")
        self.show()

    def released_button(self):
        print("This button has been released")

    def pressed_button(self):
        print("This button has been pressed")
        self.counter += 1
        self.button.setText("Clicked: {}".format(self.counter))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
