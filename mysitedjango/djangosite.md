# Start django project
Creates a bunch of files in a folder for your project

        django-admin startproject <sitename>
# Run web server
        python ./manage.py runserver
        python ./manage.py runserver <tcp port>
You can set PyCharm's run configuration parameter to include runserver so starting the server is as easy as shift-F10 for manage.py 
## Access web server
        http://127.0.0.1:8000   
## Access admin interface
        http://127.0.0.1:8000/admin/
## Create admin user
        python ./manage.py createsuperuser
        Username (leave blank to use 'par'):
        Email address: par@akerstrom.nu
        Password: avocado1
        Password (again): avocado1
        Superuser created successfully.
## Start/Create App
Creates a new directory and files for an app in your project

        django-admin startapp <app name>
        
## Re-sync db after app creation
Synchronises code changes in models  

        python ./manage.py migrate 

Set any migrations to be done?? This helps synchronise at least

        python manage.py makemigrations
        
            Migrations for 'blog':
                blog\migrations\0001_initial.py
                -Create model Post

Migrate and sync the sql db with current app settings

        python manage.py migrate --run-syncdb
      
## Manage admin settings
Use /<app>/admin.py file 

## Configure URLs
URLs for your site are set in <sitename folder>/urls.py
           
           from blog import views as blog_views
           urlpatterns = [
                path('post/', blog_views.post),
                path('', blog_views.index),
                ]
                
## webpages in html
Create a folder called "templates" in your <site> folder.
Store html files there.
refer to them without the /templates/ in your code.
Use just simply for example 'index.html'           

## Code usage in dynamic webpages
Input to this dynamic page is a list of class objects

        posts = Post.objects.all()  # Get all Post objects
        return render(request, 'index.html', {'posts': posts})  # Pass in post objects into index.html

In the dynamic webpage we can use the list of class objects in a for loop by so, and extract values like so.

        <h1>I'm an index page</h1>

        {% for post in posts %}  <!–– Use {% syntax for loops I suppose ––>
            <div>
                <h3><a href="/post/{{ post.slug }}"> {{ post.title }}</a></h3>
                <!–– Use {{ syntax for variables and methods ––>
                <p>{{ post.summary }}</p>
            </div>
        {% endfor %}
        
## Bootstrap to make it look pretty
        pip install django-bootstrap3
        
   edit <mysite>/settings.py to enable bootstrap module
   
        INSTALLED_APPS = [
        ..
        ..
        'bootstrap3'
        ]     
   
   In order for bootstrap to work, include it into the dynamic template html files
   
        {% load bootstrap3 %}
        {% bootstrap_css %}
        {% bootstrap_javascript %}

## Using stylesheets to make it look prettier - static files
In settings.py we have a static reference at the bottom.
It refers to /static/

        STATIC_URL = '/static/'

Create a folder called static in your page directory.
Then a folder called css in the static directory.
This is where css files go.

Load css in the dynamic html files by:
        {% load static %}
        <link rel="stylesheet" type="text/css" href="{% static 'css/style.css' %}">
        
## Templates, inheritance and children - Code blocks
We created a base.html with code for a generic navbar from getbootstrap.com
We moved all css and bootstrap code into this base.html template file too.

We then import the template into the other dynamic webpages by

         {% extends 'base.html' %}

To show the content of the child file, we need to define something called
block content that will be input to the template file.
So, in short we define all content on the child page as block
content. This will displayed once th user visits the child page,
whilst preserving the inherited content from the template.

        {% block content %}
            <your child html code>
        {% endblock %}
        
In the template html file, you need to specify the same blocks to display the child page code.

        {% block content %}
            <fallback html code>
        {% endblock %}
        
We can use a similar style code to set unique child page titles.

        {% block title %} 
            {{ <your-variable.title> }}
        {% endblock %}
        
## Usage of media files - media directory
This can be used to enable functionality to upload images to your site.
Open up settings, scroll to the bottom and set the media url and root directory

        MEDIA_URL = '/media/'
        MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

Then, in our models.py for the blog we add a parameter for our Post class called image.
                
        image = models.ImageField(upload_to='img')

This means that files that are uploaded will go to our media directory /media followed by /img.

---
<aside class="warning">
NOTE! Now we're not using mongodb anymore, so we can't just add fields arbitrarily to our database tables!
We're using sql lite in django so we need to address the posts that are already in the sql db that don't have the image parameter set.
</aside>

---
So remember how we initially learnt about how to synchronise our db after changes?
This is what we need to do in this case too.

        python manage.py makemigrations blog
            You are trying to add a non-nullable field 'image' to post without a default; we can't do that (the database needs something to populate existing rows).
            Please select a fix:
                1) Provide a one-off default now (will be set on all existing rows with a null value for this column)
                2) Quit, and let me add a default in models.py
            Select an option:
So the above lets you fix it during the migration or you can go back to your code in models.py and add a default field for the table value.
I chose 2 and edited models.py to contain default='placeholder.png' for the image parameter.
This would make it so that all posts without an image will use /media/placeholder.png for their image.
So we now need to upload an image in /media/ called placeholder.png.
Then, we run the command again

        $ python manage.py makemigrations blog
            Migrations for 'blog':
                blog\migrations\0002_post_image.py
                    - Add field image to post


After we've done that, we run the migration itself. Not sure what the --fake does.
        
        $ python manage.py migrate --fake blog
            Operations to perform:
                Apply all migrations: blog
                Running migrations:
                 Applying blog.0002_post_image... FAKED

        $ python manage.py migrate blog
           Operations to perform:
             Apply all migrations: blog
           Running migrations:
             No migrations to apply.

