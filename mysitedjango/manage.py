#!/usr/bin/env python
import os
import sys
import logging

logging.basicConfig(
        level=logging.INFO,
        filename='logs/blog.log',
        filemode='a',
        format='%(asctime)s %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')


name = "par"

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysitedjango.settings')
    try:
        from django.core.management import execute_from_command_line
        logging.info(f'{name} raised an info error here')
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
