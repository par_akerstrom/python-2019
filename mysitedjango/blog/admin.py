from django.contrib import admin
from .models import Post  # Import models from same directory?

# Register your models here.
admin.site.register(Post)  # Registers the class Post with the admin site