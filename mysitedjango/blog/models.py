from django.db import models
from django.urls import reverse

# Create your models here.

# We put the models class as an argument.
# This means that Post inherits the models class.
# The Post class has access to all code available in the models class.


class Post(models.Model):
    """
    superclass Post
    Defines properties of each blog post
    Inherits code from Model class
    """
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)  # slug = url
    summary = models.CharField(max_length=300)
    content = models.TextField()
    published = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='img', default='placeholder.png')

    class Meta:
        """
        sub-class
        """
        ordering = ['-created']  # sort based on date

        def __unicode__(self):
            """
            Magic Method / dunder
            Sets unicode for the posts to work
            """
            return u'%s' % self.title  # Returns the title itself into the title

    def get_absolute_url(self):
        """
        Passes in the slug (url) into the view "post"
        """
        return reverse('blog.views.post', args=[self.slug])
