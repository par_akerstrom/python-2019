from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse  # Used for simple returns like strings
from .models import Post  # Used to gain access to out Post class


# Create your views here.
def index(request):
    """
    :param request: Mandatory django input
    :return: render of dynamic webpage
    """
    posts = Post.objects.all()  # Get all Post objects
    return render(request, 'index.html', {'posts': posts})  # Pass in post objects into index.html


def post(request, slug):
    """
    :param request: Mandatory django input
    :return:
    """
    print(slug)
    return render_to_response('post.html', {
        'post': get_object_or_404(Post, slug=slug)  ## class and matching parameter
    })


def about(request):
    return render(request, "about.html", {})
