# How a generator can save memory compared to a list comprehension
# Find the first 1,000 perfect squares and sum them up.
# Now scale up to 1,000,000 squares.
# Then do 10,000,000 squares - This is when my list comp breaks my computer.
# The list comprehension basically exhaust the full memory and then crashes the whole OS.


# List comprehension returning perfect squares given a number of squares to return
def comp_for_many_perfect_squares(num):
    return sum([i * i for i in range(1, num)])


# Generator returning perfect squares. won't crash you memory.
def gen_for_many_perfect_squares(num):
    return sum((i * i for i in range(1, num)))


# Generator returning perfect squares. Will crash your memory if called like below.
def mygen(stop):
    for i in range(stop + 1):
        yield i * i


# Unsure why calling the generator externally like this causes it to use memory.
# But it definitely does, so watch out!
print(sum((mygen(1000))))
