# FizzBuzz problem
# replace all integers evenly divisible by 3 with fizz
# replace all integers evenly divisible by 5 with buzz
# replace all integers evenly divisible by both 3 and 5 with fizzbuzz

numbers = [45, 22, 14, 65, 97, 72]

for i in range(len(numbers)):
    if numbers[i] % 3 == 0 and numbers[i] % 5 == 0:
     numbers[i] = 'fizzbuzz'
    elif numbers[i] % 3 == 0:
     numbers[i] = 'fizz'
    elif numbers[i] % 5 == 0:
     numbers[i] = 'buzz'

print(numbers)

# A more elegant solution with enumerate.

numbers = [45, 22, 14, 65, 97, 72]

for index, num in enumerate(numbers, start=0):
    if num % 3 == 0 and num % 5 == 0:
        numbers[index] = 'fizzbuzz'
    elif num % 3 == 0:
        numbers[index] = 'fizz'
    elif num % 5 == 0:
        numbers[index] = 'buzz'
print(f'numbers are now {numbers}')