# Stack Data Structure Basics
A stack is a data structure that utilises Last-In/First-Out manner.
A great example is an undo/redo function in a word editor, Like this:
![stack1](pictures/stack1.png)
![stack2](pictures/stack2.png)
![stack5](pictures/stack5.png)
![stack6](pictures/stack6.png)

# Implementing a stack in Python (the built-in ways!)
We'll look at the different ways of implementing stacks in python:
* list
* collections.queue
* queue.LifoQueue

## list
Built-in lists are a prime example of a stack data structure.

### Push (.append()) and Pop (.pop())
To push an item onto a list we use the `list.append()` method.
To pop an item from a list we use the `list.pop()` method.

### Pros
Lists are very well known.
Lists are great at findings items in the middle of the stack very quickly.

### Cons
lists may suffer from speed issues when they grow. The items in a list are stored with 
the goal of giving fast access to random elements in the list. This on a high-level means
that the items are stored next to each other in memory.
If the memory required for the list grows larger than the memory block that currently holds it,
Python will have to do some memory allocations.
This memory allocation may mean that some .append() actions take longer than others, if this
particular .append() was the one causing a new allocation.

Illustration of a sample memory allocation for a list. Very efficient structure for fetching
mylist[3] for example, because python knows exactly where to find the item in memory.

In this first case we do have one slot left for a push
![list_mem1](pictures/list_mem1.png)

In this case however, our push pushes Python to allocate a new block, which takes 
some moment of time.
![list_mem2](pictures/list_mem2.png)

## collections.deque (double-ended queue)
This stack type helps us get around the list's reallocation problem
We import deque from the collections module
        
        from collections import deque

### Push (.append()) and Pop (.pop())
To push an item onto a list we use the `deque.append()` method.
To pop an item from a list we use the `deque.pop()` method.

### Pros
The deque's memory structure allows for consistent pushes, regardless of how much memory that
has been allocated.
How does it achieve that? It uses a reference memory structure, where each stack item refers
to the next entry in the stack.
This means that a push is always the same type of operation and it never requires whole blocks
of memory to be allocated. Same goes for pop, always the same operation.

![deque_mem1](pictures/deque_mem1.png)

### Cons
The negative part of the deque memory type allocation is that finding items in the stack may
require many lookups. This is because the reference of let's say my_deque[3] first goes through
my_deque[1], then my_deque[2], then my_deque[3].

## Python Stacks and Threading - queue.LifoQueue
Threading adds complexity to the mix as the previously mentioned stacks (list and deque)
are not fully thread-safe.
We may enter race conditions and our program may fail.

> **_NOTE_** lists should never be used with threading, never safe!

Deques can be used for pushing and popping in a threaded application. This is because their
documentation promises that these functions are _atomic_, meaning that they won't be interrupted
by another thread.
However, do stay careful around using deques in threaded programs as you or someone else may
expand the feature set beyond push and pop before you know it!

So if none of the above are good use cases for threading, what do we do? We use LifoQueues.

We know that stacks operate in a Last-In/First-Out principle, and that's exactly what LifoQueues are about!

We import the LifoQueues from the queue module

        from queues import LifoQueue

### Push (.put()) and Pop (.get())
To push an item onto a list we use the `LifoQueue.put()` method.
To pop an item from a list we use the `LifoQueue.get()` method.
To pop an item but not wait for a reply we use the `LifeQueue.get_nowait()` method.

### Pros
LifoQueue is designed to be fully thread-safe. It adds optional time-outs to operations
that are usually a must-have in a threaded environment. I suppose that's because you want
your worker threads to be fetching work from queues and time out and continue if there
was nothing there.

### Cons
The full thread safety comes at a slight performance cost. Each operation needs a little bit
extra care, so this will slightly decrease performance on operation. However, perhaps you've 
gained more performance by being multi-threaded so fear not straight away :).
**If** your **stack operations** are the performance bottleneck, then well maybe you need to move to
deques, if speed is of importance to you. It likely is since you used threads in the first place.

# Which implementation should you use?
For non-threaded apps the deque is a solid go to for consistent performance.
Apparently lists are not considered to the best choice for stacks because of their inconsistent
performance when it comes to memory allocation in large stacks.

Not sure how large they need to be or how to prove the performance. My tests ran quicker in a
list than in a deque!

For anything threading you would very likely want to use LifoQueues to not put your code in danger
if you would go beyond push and pop.
However, if you have measured your performance between LifoQueues and deques and you see
a performance increase for pushing and popping, you may consider using deques.
However, notice how this may include more maintenance for you.