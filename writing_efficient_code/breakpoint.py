import requests
import json


class WebPage:
    def __init__(self, url):
        self.url = url
        self.text = ""
        self.status = None

    def fetch(self, session):
        response = session.get(url=self.url)
        self.text = response.text
        self.status = response.status_code

    def convert_json(self):
        self.text = json.loads(self.text)
        return self.text

    def get_url(self):
        return self.url

    def get_status(self):
        return self.status

    def get_text(self):
        return self.text


def session_creator():
    with requests.Session() as session:
        return session


def debug_printer():
    print("Hello there, I'm a function. I've been executed just now. I'll return a False now.")
    return False

pages =[
    "https://www.lipsum.com/",
    "https://app.statuscake.com/Workfloor/Locations.php?format=json"
]


def main():
    webpages = [WebPage(page) for page in pages]
    session = session_creator()
    # Practicing use of breakpoints. Submit "continue" when you're ready in the debugger.
    # breakpoint()
    for page in webpages:
        page.fetch(session)
        print(page.get_status())
        print(page.get_status())
        print(page.get_text())
        if "json" in page.get_url():
            page.convert_json()
            print(type(page.get_text()))
    breakpoint()
    debug_printer()


if __name__ == '__main__':
    main()