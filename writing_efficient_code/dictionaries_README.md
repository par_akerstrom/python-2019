# What can be a dictionary key? Hashable Objects
Quick answer is - a hashable object.
An object is hashable if its hash value never changes during it's lifetime.
This is true for __most__ immutable objects in Python.
Always true for numbers and strings, but for example a tuple is an immutable container.
A tuple can only be a dictionary key if its objects are immutable.

# What can be a dictionary value? ANYTHING
ANYTHING. There are no restrictions in what can go into a dictionary value.

# What's a dictionary? It's a Mutable Container
I revealed the answer in the title. It's a mutable container.
As it is mutable, it's a unhashable object.
Just like lists and sets and unhashable, mutable containers.
We can't make them a key of our dictionary.

    ----> 1 mydict2 = {mydict: 1}
    TypeError: unhashable type: 'dict'
    
# Ordered vs unordered
Since Python 3.6 dictionaries are iterated in the same order they were created.
This means that they're __ordered data structures__
Be aware of this is your application is supposed to be compatible with older versions.

# Iterating over a dictionary
#### Basic
Basic for loop uses the available `.__iter__` method to loop over its __keys__.

#### New views (dynamic)
With `.items()`, `.keys()`, `.values()` methods you create dynamic views of your dict.
With dynamic we mean that they update as the dictionary changes.
> NOTE these are highly memory efficient generator views!

`.items()` obviously gives you key/value pairs.
`.keys()` and `.values()` give you keys and values.

> NOTE! You can't change the values of you views! But you can use your views to change the original dictionary!

#### Modifications in iteration
We can modify the original dictionaries values by using our view like this.
Note how we use prices[k] to modify the original.

    >>> prices = {'apple': 0.40, 'orange': 0.35, 'banana': 0.25}
    >>> for k, v in prices.items():
    ...     prices[k] = round(v * 0.9, 2)  # Apply a 10% discount
    ...
    >>> prices
    {'apple': 0.36, 'orange': 0.32, 'banana': 0.23}
    
Crazy enough though, you can access the actual dictionary if you make a list of the items view.
This makes it possible to delete values if you want!
This consumes some memory as you're loading the entire dictionary into a list, just a word of warning
if the dictionary is humongous.

    for key, value in list(cowboy.items()):
        if key == 'hat_size':
            del cowboy[key]
            
# Real World Examples
If for some reason we want to make the keys into values, and vice versa.
We can do so by creating a new empty dictionary and assign values in a for loop.

    new_cowboy = {}
    for key, value in cowboy.items():
        new_cowboy[value] = key
    
    print(new_cowboy.items())

#### Math operations on values in a dictionary
Easy-peasy really.

    >>> incomes = {'apple': 5600.00, 'orange': 3500.00, 'banana': 5000.00}
    >>> total_income = 0.00
    >>> for value in incomes.values():
    ...     total_income += value  # Accumulate the values in total_income
    ...
    >>> total_income
    14100.0
    
#### Dictionary Comprehension

