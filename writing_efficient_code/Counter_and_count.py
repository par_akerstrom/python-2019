# For complex counting we can use Counter. It can ingest hashable objects (strings, integers, tuples with only
# hashable objects in them) and give you a perfect count for each element!

from collections import Counter

words = 'All the in in in the entire world can be put into this string, hey hey hey'.split()

counts = Counter(words)  # Count and store each word in the list along with a count of occurrences
print(counts)

print(counts.most_common(3))  # Displays the top three most common occurrences

# For smaller use cases we can use the simple list.count(element) method.
print("How many 'All' do we find in the sentence?", words.count('All'))

# Can effectively be used in the sock problem with a generator(?)
# Each sock has a colour code, 10/20/30/50.
# How many combinations of socks match?
colour_socks = [10, 20, 20, 10, 10, 30, 50, 10, 20]

# Make a set() to get the number of iterations right in the generator
# colour_socks.count(x) counts the instances of current value
# //2 returns the amount of times 2 fits into the number.
# sum() sums it all up into the correct value - 3.
match_two_colours = (sum(colour_socks.count(x)//2 for x in set(colour_socks)))
print(f'This is how many combos of matching two of the same int: {match_two_colours}')

# Counting in an alternate way? A super tricky but smart way of counting a dict is with a default dict.
from collections import defaultdict

food_list = 'spam spam spam spam spam spam eggs spam'.split()
food_count = defaultdict(int) # default value of int is 0
for food in food_list:
    food_count[food] += 1 # increment element's value by 1
print(food_count)

# This is exactly the same as counter though, not sure the gain.
food_counter = Counter(food_list)
print(food_counter)