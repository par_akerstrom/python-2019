# Dictionary Comprehension - combination of two lists with zip!
# Refresh on zip if necessary. zip creates a generator like zip object that spits out tuples until exhausted.
# Note how comprehensions create a new dict, it's not a dynamically linked view anymore!

# First, we create lists for keys and values, using list comprehensions, just for fun.
mykeys = ['mykey' + str(x) for x in range(1, 6)]  # list1
myvalues = ['myvalue' + str(x) for x in range(1, 6)]  # list2

# And here's the dictionary comprehension! We're generating tuple pairs with the zip, exhausting to the lowest common
# denominator into the dictionary.
mydictionary = {key: value for key, value in zip(mykeys, myvalues)}
print(f'mydictionary is: {mydictionary}')

# With the new knowledge of dictionary comprehension we can address problems in a new way.
# Let's return to making keys to values and vice versa. So powerful and so pythonic now!
mydictionary_reverse = {key: value for value, key in mydictionary.items()}
print(mydictionary_reverse)

# Of course the same filters can be applied. Here we do a silly restraint on the 8th character in the string
mydict_filtered = {key: value for key, value in mydictionary.items() if int(value[7]) < 3}
print(mydict_filtered)

# Note how you don't need to use two variables to generate a dictionary comprehension.
# You can just refer to the original dictionary, like this:
mydictionary_selected = {k: mydictionary[k] for k in mydictionary.keys()}
print('mydictionary_selected: ', mydictionary_selected)

# With the above method, what's happening is that we get a dynamic key_view, refresh dictionary views in the
# dictionary section if needed.
# The view acts much like a set with only hashable, unique objects.
# So this means that we can perform common `set` operations on the key_view.
# Among them are, well subtraction. So we can simply subtract keys as we wish with the following syntax.
mydictionary_selected = {k: mydictionary[k] for k in mydictionary.keys() - {'mykey1'} - {'mykey3'}}
print('mydictionary selected minus certain keys: ', mydictionary_selected)

