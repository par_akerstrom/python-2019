# How many combinations of your friends can be next to each other on a bus ride to
# the blue mountains? There are two seats next to each other on each side of the bus.

from itertools import permutations, combinations

friends = ['Ashley', 'Romy', 'Jamie', 'Dumbo', 'Alladin']

# Create a permutation object. r= sets the grouping size.
# Note that ['Ashley', 'Romy'] is different from ['Romy', 'Ashley']
list_of_perm = permutations(friends, r=2)

# Create a combination object. r= sets the grouping size.
# Note that ['Ashley', 'Romy'] is not different from ['Romy', 'Ashley']
list_of_comb = combinations(friends, r=2)

print(f'objects are:\n{type(list_of_perm)}\n{type(list_of_comb)}')

# To print out the values, convert to a list.
print(f'{list(list_of_perm)}\n{list(list_of_comb)}')

print(f'{len(list(list_of_perm))}\n{len(list(list_of_comb))}')
