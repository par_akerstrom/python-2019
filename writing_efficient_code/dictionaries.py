""" BASICS """
# Find the name of our cowboy. If that name exists, store it in the name variable.
# if it doesn't exist, set the name to the man with no name.

cowboy = {'age': 27,
          'horse': 'mustang',
          'hat_size': 'large',
          }

# Explicit method
if 'name' in cowboy:
    name = cowboy['name']
else:
    name = 'The man with no name'
print(name)

# Elegant method with .get()
name = cowboy.get('name', 'The man without any name')
print(name)

# Check if key 'name' exists in cowboy, it it doesn't, set it to "The man with no name"

# Explicit way
if 'name' not in cowboy:
    cowboy['name'] = 'The man with no name'
name = cowboy.get('name')
print(name)

# Elegant way with .setdefault. It does exactly the same thing as the above code.
# I just chose to search for the 'middle name' key instead, to prove the point of
# that .setdefault() will actually add the value if it doesn't exist already.
name = cowboy.setdefault('middle name', 'Middle name for a man without a Name')
print(cowboy.get('middle name'))


""" VIEWS AND ITERATION """

# A dynamic iteration method for dictionaries, .items()
# This will update as the dictionary changes!
item_view = cowboy.items()
print(f'my new view of cowboy:{item_view}')

# Pretty good for iteration to see both keys and values
for item in item_view:
    print(item, type(item), ('items type = tuple'))  # Tuple!

# These are actually as you can see above tuples.
# We can hence unpack the tuples quite pythonically
for key, value in item_view:
    print(key, '->', value)

# .keys() give us a new key view, cool! Could be any hashable object type, remember?
key_view = cowboy.keys()
for key in key_view:
    print(key, type(key), '(key type = only hashable objects)')

# .values() gives us a new values view, cool! Can be any object type.
values_view = cowboy.values()
for value in values_view:
    print(value, type(value), '(value type = any type)')

# The above are good for membership tests
print('hat_size' in cowboy.keys())
print('small' in cowboy.values())

# To modify values in a view, we must refer to the original dictionary as seen here.
# This is because the view is just a reference to the original.
for key, value in cowboy.items():
    if key == 'hat_size':
        cowboy[key] = 'small'

# Crazy enough though, you can access the actual dictionary if you make a list of the items view.
# This makes it possible to delete values if you want!

for key, value in list(cowboy.items()):
    if key == 'hat_size':
        del cowboy[key]

print(cowboy.items())

# If for some reason we want to make the keys into values, and vice versa
new_cowboy = {}
for key, value in cowboy.items():
    new_cowboy[value] = key

print(new_cowboy.items())

# If we want to be selective
new_cowboy = {}
for key, value in cowboy.items():
    if value == 'mustang':
        new_cowboy[key] = value
print(new_cowboy.items())

""" CALCULATIONS """

# We can do "calculations" or here I made it into "String calculation", adding strings.
# In two ways, one with .join as I need to practice that and one in traditional list way.
combined = ''
combined_list = []
for value in cowboy.values():
    combined = combined + str(value) + ' '
    combined_list.append(str(value))

combined_string = ' '.join(combined_list)
print(combined)
print(combined_string)

# Addition now that we can write more pythonic code
sales = {'apple': 600, 'banana': 1000, 'lemon': 200}
print(sum([x for x in sales.values()]))  # List comprehension way
print(sum((x for x in sales.values())))  # Generator way, saving memory!
print(sum(sales.values()))  # Direct way, SO simple.


""" COMPARISONS """

importers = {'El Salvador' : 1234,
             'Nicaragua' : 152,
             'Spain' : 252
            }

exporters = {'Spain' : 252,
             'Germany' : 251,
             'Italy' : 1563
             }

print(importers.items() & exporters.items(), "exist in both dicts")  # Find items in both dicts
print(importers.keys() - exporters.keys(), "keys unique to importers dict")

# Comparing dictionaries and we pay attention to the individual values for each key
for key, value in importers.items():
    if key in exporters.keys():
        if value <= exporters.get(key):
            print("exporters have an equal or higher value in this key/value pair")
            print('Key:', key, 'Value:', exporters.get(key))