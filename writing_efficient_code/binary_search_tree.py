class Node:
    def __init__(self, val):
        self.value = val
        self.leftChild = None
        self.rightChild = None

    def insert(self, data):
        if self.value == data:  # Check if the data already exists in true, if so, don't add it again.
            return False
        elif self.value > data:  # Check if less than current node
            if self.leftChild:  # Check if left node child exists.
                return self.leftChild.insert(data)  # Recursion call to leftChild's .insert method.
            else:
                self.leftChild = Node(data)  # If it didn't exist, create left child
                return True
        else:
            if self.rightChild:  # if great than current
                return self.rightChild.insert(data)  # recursion call to rightChild's .insert method.
            else:
                self.rightChild = Node(data)  # if it didn't exist, create right child
                return True

    def find(self, data):
        if self.value == data:
            return True
        elif self.value > data:
            if self.leftChild:
                return self.leftChild.find(data)
            else:
                return False
        else:
            if self.rightChild:
                return self.rightChild.find(data)
            else:
                return False

    def preorder(self):
        if self:
            print(str(self.value))
            if self.leftChild:
                self.leftChild.preorder()
            if self.rightChild:
                self.rightChild.preorder()

    def postorder(self):
        if self:
            if self.leftChild:
                self.leftChild.postorder()
            if self.rightChild:
                self.rightChild.postorder()
            print(str(self.value))

    def inorder(self):
        if self:
            if self.leftChild:
                self.leftChild.inorder()
            print(str(self.value))
            if self.rightChild:
                self.rightChild.inorder()


class Tree:
    def __init__(self):
        self.root = None

    def insert(self, data):
        # breakpoint()
        if self.root:  # Check if a root node exists, if so call .insert method on the root object
            return self.root.insert(data)
        else:  # If root node doesn't exist, create root node.
            self.root = Node(data)
            return True

    def find(self, data):
        if self.root:
            return self.root.find(data)
        else:
            return False

    def preorder(self):
        print('PreOrder')
        self.root.preorder()

    def postorder(self):
        print('PostOrder')
        self.root.postorder()

    def inorder(self):
        print('InOrder')
        self.root.inorder()


if __name__ == '__main__':
    bst = Tree()  # instantiate Tree object
    # breakpoint()
    print(bst.insert(1200)) # Create root node, value 1200
    print(bst.insert(15))  # compare to 1200, create left node
    print(bst.insert(10))  # compare 1200, recursion to left node, compare to 15 and create left node
    print(bst.insert(1600)) # compare to 1200, create right node
    print(bst.insert(99))  # compare to 1200, recursion to left node, compare to 15, create right node.
    # breakpoint()
    bst.preorder()  # Top down left scan
    bst.postorder()  # bottom up order, left to right
    bst.inorder()  # if no left, print yourself and go to the right. Will get smallest to biggest.

    # Create a thousand nodes. Had to divide and conquer to avoid maximum recursion depth.
    for _ in [50*x for x in range(1, 21)]:
        for _ in range(_-49, _):
            bst.insert(_)
