def get_name_and_age(name, age):
    # With f-strings we can get very readable code.
    # We can evaluate expressions at runtime, like this calculation
    # Or like the list comprehension! Woah, let's not go too crazy with this!
    return f'My name is {name} and my age is {age / 10:.1f} decades old.\n ' \
        f'My name is spelled {[x for x in name]}'


print(get_name_and_age('par', 34))

# We can chop up strings like this, for whatever use it has
my_string = 'This is a string that I will chop up      '
print(f'my chopped up string from character 0 to 7 is: {my_string[0:7]}')

# rstrip() can strip off trailing characters in a string, default is whitespace
print(my_string.rstrip())
