from collections import deque
from queue import LifoQueue
from time import perf_counter
from random import randint

class StackHolder:
    def __init__(self):
        self.mystack = []
        self.list_stack()
        self.my_deque = deque()
        self.deque_stack()
        self.my_lq = LifoQueue()
        self.lifoqueue_stack()

    def list_stack(self):
        start = perf_counter()
        # self.mystack = [push for push in "abc"] * 2 ** 25
        self.mystack = [randint(1000, 2000) for _ in range(2 ** 5)]

        elapsed = perf_counter() - start
        print(f'Allocation of list stack took {elapsed:0.4f} second/s')

        start = perf_counter()

        for _ in range(2 ** 5):
            self.mystack.append(randint(1000, 2000))
        elapsed = perf_counter() - start
        print(f'Allocation of list stack took {elapsed:0.4f} second/s')

        print(f'Length of the list stack is now: {len(self.mystack)}')

        start = perf_counter()
        for i in range(len(self.mystack)):
            try:
                self.mystack.pop()
            except IndexError as e:
                print(e)
            except BaseException as e:
                print(e)
        elapsed = perf_counter() - start
        print(f'Popping list stack took {elapsed:0.4f} second/s')

    def deque_stack(self):
        start = perf_counter()
        # self.my_deque = deque(push for push in "abc" * 2 ** 25)
        self.my_deque = deque(randint(1000, 2000) for _ in range(2 ** 5))
        elapsed = perf_counter() - start
        print(f'Allocation of deque stack took {elapsed:0.4f} second/s')

        start = perf_counter()
        for _ in range(2 ** 5):
            self.my_deque.append(randint(1000, 2000))
        elapsed = perf_counter() - start
        print(f'Allocation of deque stack took {elapsed:0.4f} second/s')

        print(f'Length of the deque stack is now {len(self.my_deque)}')

        start = perf_counter()
        for i in range(len(self.my_deque)):
            try:
                self.my_deque.pop()
            except IndexError as e:
                print(e)
            except BaseException as e:
                print(e)

        elapsed = perf_counter() - start
        print(f'Popping deque stack took {elapsed:0.4f} second/s')

    def lifoqueue_stack(self):
        start = perf_counter()
        self.counter = [push for push in "abc"] * 2 ** 5

        for item in self.counter:
            self.my_lq.put(item)

        elapsed = perf_counter() - start
        print(f'The LifoQueue stack is now {len(self.counter)}')
        print(f'Allocation and adding to LifoQueue took {elapsed:0.2f} second/s')
        try:
            print(f'Length of the LifoQueue is now {len(self.my_lq)}')
        except BaseException as e:
            print(f"We can't measure lifoqueue length like this, because {e}")

        start = perf_counter()
        for i in range(len(self.counter)):
            self.my_lq.get()
        elapsed = perf_counter() - start
        print(f'Popping the LifoQueue took {elapsed:0.2f} second/s')
        try:
            print(self.my_lq.get_nowait())
        except IndexError as e:
            print(e)
        except BaseException as e:
            print(e)


if __name__ == '__main__':
    StackHolder()
