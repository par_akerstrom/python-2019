# Call get_random_word() repeatedly to get 1,000 words.
# Return a data structure containing every unique word

from time import perf_counter
import random

all_words = 'all the words in the world'.split()


def get_random_word():
    return random.choice(all_words)


def get_set():
    myset = set()
    for i in range(10000000):
        myset.add(get_random_word())  # Super efficient way of doing comparison
    return myset


def get_set_comprehension():
    # Super efficient and pythonic way of writing it (set comprehension)
    myset = set(get_random_word() for _ in range(10000000))
    return myset


def get_slow_version_with_list():
    words = []
    for _ in range(10000000):
        word = get_random_word()
        if word not in words:  # This method with a list is slightly slower
            words.append(word)
    return words


start = perf_counter()
print(get_set())
elapsed = perf_counter() - start
print(f'set with for loop took {elapsed:0.2f} second/s\n')

start = perf_counter()
print(get_set_comprehension())
elapsed = perf_counter() - start
print(f'set with comprehension took {elapsed:0.2f} second/s\n')

start = perf_counter()
print(get_slow_version_with_list())
elapsed = perf_counter() - start
print(f'list with for loop took {elapsed:0.2f} second/s\n')
