# ipython magic function %timeit - Simple time measurement
    In [1]: %timeit fact(50)
    8.76 µs ± 298 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
     
    In [2]: %timeit fact2(50)
    18.2 µs ± 1.12 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)

# Big Order Notation
Execution time is not always a good metric to measure the complexity of an algorithm since it depends upon the
hardware. A more objective complexity analysis metrics for the algorithms is needed. This is where Big O notation
comes to play.

#### The most common Big-O functions:
|Name|Big O|
|:---:|:---:|
|Constant| O(c)|
|Linear    |O(n)|
Quadratic| O(n^2)
Cubic  |O(n^3)
Exponential|O(2^n)
Logarithmic    |O(log(n))
Log Linear |O(nlog(n))

Here's a Constant Complexity, (O(C))

    def constant_algo(items):  
    result = items[0] * items[0]
    print ()

    constant_algo([4, 5, 6, 8])  

In the above script, irrespective of the input size, or the number of items in the input list items the algorithm
performs only 2 steps:
Finding the square of the first element and printing the result on the screen.
Hence, the complexity remains constant.

![constant_complexity](pictures/constant_complexity.png)

Here's a linear complexity algo, O(n)

    def linear_algo(items):  
    for item in items:
        print(item)

    linear_algo([4, 5, 6, 8])  

The iterations of the for loop is equal to the input length.

![linear_complexity](pictures/linear_complexity.png)

And lastly here's a quadratic complexity function, O(n^2).
Iterations scale exponentially with input length.

    def quadratic_algo(items):
        for item in items:
            for item2 in items:
                print(item, ' ', item2)
    
    
    quadratic_algo([4, 5, 6, 8])

![quadratic_complexity](pictures/quadratic_complexity.png)
    
# Finding the complexity of a complex function
Given the below function, what's its complexity?

    def complex_algo(items):
    
        for i in range(5):  # This is constant complexity O(5)
            print ("Python is awesome")
    
        for item in items:  # This is linear complexity O(n)
            print(item)
    
        for item in items:  # This is linear complexity O(n)
            print(item)
    
        print("Big O")  # This is constant complexity O(3)
        print("Big O")
        print("Big O")
    
    complex_algo([4, 5, 6, 8])     

To calculate this we break it into sections and perform simple addition for each section.

O(5) + O(n) + O(n) + O(3)  = O(8) + O(2n)
We know that the constants are irrelevant when we move towards infinity.
(Half infinity is still infinity). As such, we can ignore the constants.
The summary output is as such O(n)!

# Worst case complexity, always
Below example shows a function that exits when a value is found.
We will always assume that the maximum amounts of attempts are required for the value to be found.
As such, we assume we have to iterate through the entire list. As such the complexity will be linear, O(n)

    def search_algo(num, items):  
        for item in items:
            if item == num:
                return True
            else:
                return False
    nums = [2, 4, 6, 8, 10]
    
    print(search_algo(2, nums))  