from random import randint


def rnd_gen():
    rand_num = randint(10, 20)
    for line in range(1, rand_num):
        yield randint(1, 100)


animals = [
    {'type': 'penguin', 'name': 'Stephanie', 'age': 8},
    {'type': 'elephant', 'name': 'Devon', 'age': 3},
    {'type': 'puma', 'name': 'Moe', 'age': 5},
]

print(__name__)

if __name__ == '__main__':
    my_random_list = list(rnd_gen())
    print(f'Random list of integers: \n{my_random_list}\n')
    print(f'Now sorted: \n{sorted(my_random_list)}\n')
    print(f'Now reverse-sorted: \n{sorted(my_random_list, reverse=True)}\n')

    # More complex structure of nested lists can be sorted with lambda
    mylist = [['Harry', 37.21], ['Berry', 37.21], ['Tina', 37.2], ['Akriti', 41], ['Harsh', 39]]
    mylist_sorted = sorted(mylist, key=lambda x: x[-1])  # Sorts on last element in the nested lists

    print("Unsorted list of dictionaries:")
    for animal in animals:
        print(animal)

    # Slightly more complex sorting, based on a specific key in the dictionary
    print("\nsorted list of dictionaries:")
    animals_sorted = sorted(animals, key=lambda animal: animal['age'])
    for animal in animals_sorted:
        print(animal)
