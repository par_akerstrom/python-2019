# To not have to remember ascii numbers manually for checks we can import the string module
# It helps us by having predefined values for all things string.
import string


def is_upper(word):
    for letter in word:
        if letter not in string.ascii_uppercase:
            return False
    return True


print('Is LOL all uppercase?', is_upper('LOL'))
print('Is "LOL LOL" all uppercase?', is_upper('LOL LOL'))
print('Is "LOL LOL" all uppercase?', 'LOL LOL'.isupper())

print('Is Hello THERE all uppercase?: ', is_upper('Hello THERE'))

# These are other strings that are found in the string module.
print(string.ascii_uppercase)
print(string.ascii_letters)
print(string.hexdigits)
print(string.punctuation)


# String checks can also be made with built in functions on the string

str.isalnum('abcABC123')
#Return true if all characters in the string are alphanumeric and there is at least one character, false otherwise.

str.isalpha('abcABC123')
#Return true if all characters in the string are alphabetic and there is at least one character, false otherwise.

str.isdigit('abcABC123')
#Return true if all characters in the string are digits and there is at least one character, false otherwise.

str.islower('abcABC123')
#Return true if all cased characters 4 in the string are lowercase and there is at least one cased character,
# false otherwise.

str.isspace('abcABC123')
#Return true if there are only whitespace characters in the string and there is at least one character, false otherwise.

str.istitle('abcABC123')
#Return true if the string is a titlecased string and there is at least one character, for example uppercase
# characters may only follow uncased characters and lowercase characters only cased ones. Return false otherwise.

str.isupper('abcABC123')
#Return true if all cased characters 4 in the string are uppercase and there is at least one cased character,
# false otherwise.