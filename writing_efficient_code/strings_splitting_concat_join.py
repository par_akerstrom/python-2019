# Strings are sequenced objects, so we can grab indexes, lengths etc.
# Strings are IMMUTABLE. So whenever you "change" a string, you create a new one.

# split(separator) creates a list of strings separated by the separator.
# Default separator is whitespace (find them in ascii if you want)
# Note how the separators are REMOVED in the split.
my_list_of_strings = "These.are my words in my.string".split('.')
print(my_list_of_strings)

# Maxsplit can be used to limit the amount of splits
my_list_of_strings = "These are my words in my string".split(maxsplit=6)
print(my_list_of_strings)

# Simple way to do a word count on a long list of strings
print('word count is:', len(my_list_of_strings))


# Joining strings together is a reverse of the split, we use ''.join()
# Just start with your separator which should be a string, then your .join and throw an iterable as an argument.
# '<separator>'.join(iterable)
my_string_of_list_items = ' my_sep '.join(my_list_of_strings)
print(my_string_of_list_items)
