# A set is an unordered collection of items. Every element is UNIQUE (no duplicates) and must be immutable (which cannot be changed).
# However, the set itself is mutable. We can add or remove items from it.
# Sets can be used to perform mathematical set operations like union, intersection, symmetric difference etc.

# Definition of sets are made with curly braces or with set().
# Only one item can be taken as an argument in the set() function though. So if you want a whole list in there, toss a list at it.
my_set_a = {1, 2, 3, 4, 5}  # A set of integers
my_set_b = {4, 5, 6, 7, }  # A set of integers
my_set_c = set("string")  # A set of characters from a string  {'t', 'n', 'g', 's', 'r', 'i'}

print(f"We start out with these three sets: \nset a: {my_set_a}\nset b: {my_set_b}\nset c: {my_set_c}\n")

# We can add single element using the add() method and multiple elements using the update() method.
my_set_a.add(6)

# The update() method can take tuples, lists, strings or other sets as its argument. In all cases, duplicates are avoided.
my_set_b.update([8, 9, 10, 11])
my_set_c.update('new')

# discard removes an element from the set if it is a member. (Do nothing if the element is not in set)
# remove raises an error if the value is not found.
my_set_a.discard(6)
my_set_b.remove(10)

print(f"Our modified sets: \nset a: {my_set_a}\nset b: {my_set_b}\nset c: {my_set_c}\n")


def existence_in_set(variable_to_check, set_for_check):
    # Check for existence in a set
    # Returns true or false
    return variable_to_check in set_for_check


def set_union(set_1, set_2):
    '''
    A union is a set (unique representation) of all elements in two sets
    '''
    return set_1 | set_2


def set_intersection(set_1, set_2):
    '''
    An intersection is a set (unique representation) of elements found in both sets
    '''
    return set_1 & set_2


def set_difference(set_1, set_2):
    '''
    A difference is a set of elements unique to the first set. A presentation of subtracting one set from another.
    '''
    return set_1 - set_2


def set_symmetric_difference(set_1, set_2):
    '''
    Symmetric difference represents a set of elements that exist in set_1 and set_2, but elements that are common in both sets are excluded.
    This is the inverse of intersection
    '''
    return set_1 ^ set_2


print("Does the variable exist in my set? ", existence_in_set('g', my_set_c))
print("What's the union for my two sets? ", set_union(my_set_a, my_set_b))
print("What's the intersection for my two sets? ", set_intersection(my_set_a, my_set_b))
print("What's the difference for my two sets? ", set_difference(my_set_a, my_set_b))
print("What's the symmetric difference for my two sets? ", set_symmetric_difference(my_set_a, my_set_b))

# We can also use the beautiful built in methods, which makes more sense to me!
# Exactly the same as above, just with words instead of symbols.
print("What's the union for my two sets? ", my_set_a.union(my_set_b))
print("What's the intersection for my two sets? ", my_set_a.intersection(my_set_b))
print("What's the difference for my two sets? ", my_set_a.difference(my_set_b))
print("What's the symmetric difference for my two sets? ", my_set_a.symmetric_difference(my_set_b))

# There are so many powerful built-in functions for sets.
print(all(my_set_a))  # Return True if all elements of the set are true (or if the set is empty).
print(any(my_set_a))  # Return True if any element of the set is true. If the set is empty, return False.
print(min(my_set_a))  # Return the smallest item in the set.
print(max(my_set_a))  # Return the largest item in the set.
print(sorted(my_set_a))  # Return a new sorted list from elements in the set(does not sort the set itself).
print(sum(my_set_a))  # Return the sum of all elements in the set.

enum = enumerate(
    my_set_a)  # Return an enumerate object. It contains the index and value of all the items of set as a pair.
for line in enum:
    print(line)