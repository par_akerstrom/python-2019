from time import perf_counter


# Calculation of factorial. Fairly sure this is a Linear algorithm, O(n)
def fact(n):
    product = 1
    for i in range(n):
        product = product * (i + 1)
    return product


start = perf_counter()
fact(100)
elapsed = perf_counter() - start
print(f'fact: {elapsed:0.8f}')


# Calculation of factorial with recursion. Slightly unsure, but believe this is O(n2) due to the recursion.
# We also see the execution time gap going up the higher number we use, perhaps indicating increasing execution time.
def fact2(n):
    if n == 0:
        return 1
    else:
        return n * fact2(n - 1)


start2 = perf_counter()
fact2(100)
elapsed2 = perf_counter() - start
print(f'fact2: {elapsed2:0.8f}')

if elapsed > elapsed2:
    print(f'Elapsed: {elapsed:0.5f} took longer than elapsed2{elapsed2:0.5f}')
else:
    print(f'Elapsed2: {elapsed2:0.5f} took longer than elapsed {elapsed:0.5f}')


# Definition of a constant complexity algorithm, O(c). Doesn't change regardless of input.
def constant_algo(items):
    result = items[0] * items[0]
    return result


constant_response = constant_algo([4, 5, 6, 8])


# Linear complexity function O(n). Iterations scale linearly with the input length.

def linear_algo(items):
    for item in items:
        return (item)


linear_response = linear_algo([4, 5, 6, 8])


# Quadratic Complexity function O(n^2). Iterations scale exponentially with input length.

def quadratic_algo(items):
    for item in items:
        for item2 in items:
            return item, ' ', item2


quadratic_response = quadratic_algo([4, 5, 6, 8])
