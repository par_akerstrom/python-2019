# Iterate with enumerate() instead of range()
It can be more elegant to utilise enumerate. 
One of the cases for this is the fizzbuzz problem where you need access to both the index
and the value for each iteration. With enumerate, we have that direct access.
This makes it possible for us to look straight at the value, without using the index to get the
value, saving us a few characters of code. Perhaps some processing time too?

# List comprehensions instead of map() and filter()
List comprehensions are at the core of Python's easy to read and use syntax.
Although map() and filter() may be more like other language's functions, they are harder to
intuitively understand.
Use list comprehensions for a pythonic code style and readability, it counts.

# Use Generators - to save memory
The awesome list comprehensions are convenient but can sometimes lead to unnecessary
and dangerous memory usage.

Like in this example of a list comprehension for perfect squares.
        
        def comp_for_many_perfect_squares(num):
            return sum([i * i for i in range(1, num)])
            
We will use it to find We will find the first 1,000,000 perfect squares         
            
        In [59]: %time sum([i * i for i in range(1, 1000000)])                                                   
        CPU times: user 75.8 ms, sys: 16.6 ms, total: 92.4 ms
        Wall time: 90.4 ms
        Out[59]: 333332833333500000

Ok that was quite fast. Let's try 10,000,000. During this example I observed the RAM usage
on my laptop go up to about 25%.
        
        In [60]: %time sum([i * i for i in range(1, 100000000)])                                                 
        CPU times: user 6.15 s, sys: 937 ms, total: 7.09 s
        Wall time: 7.08 s
        Out[60]: 333333328333333350000000
       
 What about 100,000,000? Well, I can't show the output because it quite quickly crashes my
 laptop. This is the last we saw before the whole OS crashed
 
 ![list_comp_memory_exhaustion](pictures/list_comp_mem_exhaustion.png)
 
Ok so that wasn't great, we can't load the full data set into memory anymore, which is what
the list comprehension is trying to do when sum wants to calculate the summary.

One strategy of working around it is to load only partial into memory, which is exactly what
a generator can do for us.
Instead of loading up the whole list, the generator gives us a generator object.
What happens then in our example is that sum() is calling next(), next(), next() on the 
object to sum up all values. Each instance of next returns their value to sum and increment
the total sum. Only one element exists in memory at any time and can now work on massive sequences
of data.

So how does it look with a generator? Not much different!
We simply use round brackets instead of square brackets and voila it becomes a generator
expression instead of a list comprehension.

        def gen_for_many_perfect_squares(num):
            return sum((i * i for i in range(1, num)))

What happens now when we load up 10,000,000 perfect squares for calculation?
It goes up to 100% CPU as expected, but there's no memory exhaustion. RAM stays at 0.4%.

        In [13]: %time gen_for_many_perfect_squares(1000000001)                         
        CPU times: user 1min 24s, sys: 65.5 ms, total: 1min 24s
        Wall time: 1min 24s
        Out[13]: 333333333833333333500000000

![generator_memory_save](pictures/generator_memory_save.png)


# Use f-strings, always (except for user-supplied data :))
f-strings are super readable. 
They are also very flexible as you can evaluate expressions at runtime, like calculations or
even list comprehensions!

        return f'My name is {name} and my age is {age / 10:.1f} decades old.\n ' \
               f'My name is spelled {[x for x in name]}'

The only time where f-strings may not be fit is when you are you output user-generated values.
AS the f-strings can evaluate expressions and all sorts of things this may be a security risk.
For outputting user-generated values, it may be a better choice to use Template Strings.

Template strings don't allow for format specifiers which can be an attack vector against your code.
You have to manually substitute the format in the template strings, which is good in this case.

![which_string_format](pictures/which_string_format.png)   

# sorted() - your go to sorter
sorted() can efficiently sort any iterable for you.
Use the `reverse=True` keyword to reverse the sorting, so easy!

It can also tackle more complex data structures, like a list of dictionaries.
So I guess like an unpacking and understanding of what's inside the list's values (each dictionary).
We can choose which key to sort on by the nifty use of a lambda function.
Fairly straight forward, and so handy if this is what you need!
Given the below code, we will sort a list of dictionaries, based on the key 'age', that luckily
exists in all the dictionaries. Otherwise we'd probably get a key error.

        animals = [
                    {'type': 'penguin', 'name': 'Stephanie', 'age': 8},
                    {'type': 'elephant', 'name': 'Devon', 'age': 3},
                    {'type': 'puma', 'name': 'Moe', 'age': 5},
                ]

        animals_sorted = sorted(animals, key=lambda animal: animal['age'])

        {'type': 'elephant', 'name': 'Devon', 'age': 3}
        {'type': 'puma', 'name': 'Moe', 'age': 5}
        {'type': 'penguin', 'name': 'Stephanie', 'age': 8}

# Use sets for unique values - because data structure
Sets have their elements stored in a manner that allows for near-constant-time checks for checking
whether a value is already in the list or not.
This is incredibly efficient when you're reading in values that you suspect are duplicates.

Without providing any technical details we know that if we check a list for duplicate values python
uses a less optimal algorithm to compare the new value against every other value in the list,
compared to a set.

            if word not in words:  # A lot slower than adding the value straight to a set!
                words.append(word)

# Define values in Dictionaries with `.get()` and `.setdefault()`.
There are particularly elegant syntaxes for finding and setting single values in a dict.
We usually don't need to do this explicitly, but devs often do.
It's a good habit to use these inexplicit, elegant methods.
> Note for multiple keys at the same time, check out `collection.defaultdict()`

So for `.get()` we look through the dict if the key exists. We get the value if it existed.
If the key didn't exist, we instead get our specified return value instead.
In this case, the return default value in our call was set to 'The man without any name'

        name = cowboy.get('name', 'The man without any name')
        
OK great so that's for getting a value, how about setting a value?
We should put `.setdefault()` into normal practice.
What it does is that it searches the dict for the key specified. If the key doesn't exist,
it creates it and loads the default value specified in our call into the dictionary.
It also return that value so you can save it in a variable.

In this example we're searching the dict for the 'middle name' key. If it doesn't exist,
create it and put the default value into it, 'Middle name for a man without a Name'

        name = cowboy.setdefault('middle name', 'Middle name for a man without a Name')

        

# Take Advantage of Python's Standard Library
There's loads and loads of module in the standard library that are just an import statement away.
Using these modules can supercharge your skills.
A few handpicked standard library modules will follow.

## Extending dictionary functionality with `collections.defaultdict()`
`.get()` and `.setdefault()` work well when you're setting a default value for a single key in a
dictionary.

Pretend you have a group of students and you need to keep track of their grades on homework assignments.
Input is a list of tuples with the format (student_name, grade). 
Now let's say that you want to look up all the grades for a single student without iterating over the list.

That can be done by this approach. We don't know if the student_grades['name'] exist
or not when we loop over it, so we have to check for it.

        >>> student_grades = {}
        >>> grades = [
        ...     ('elliot', 91),
        ...     ('neelam', 98),
        ...     ('bianca', 81),
        ...     ('elliot', 88),
        ... ]
        >>> for name, grade in grades:
        ...     if name not in student_grades:
        ...         student_grades[name] = []
        ...     student_grades[name].append(grade)
        ...
        >>> student_grades
        {'elliot': [91, 88], 'neelam': [98], 'bianca': [81]}

Well, I can't say I fully comprehend the implementation of the `collections.defaultdict()`
but what it does in broad terms is that it expands the functionality of a normal dictionary.
With the expanded functionality we can define a constructor for the dictionary.

        from collections import defaultdict
        student_grades = defaultdict(list)

What this essentially achieves is that it makes it possible for us to act as if the keys were
always present. Don't ask me about the technicalities around that for now. That'll be later on
in this journey.

However, with us being able to act as if the key is always present, we can simply append straigh
away to our `.defaultdict()`. We don't have to deal with the whole empty list for each student!

        for name, grade in grades:
            student_grades[name].append(grade)
            
# Count hashable objects with `colletions.Counter()`
Isn't it nice to be able to count a hashable object with a one-liner?
The answer is yes and the answer is `collections.Counter()`.
We import Counter from collections. All we have to do is simply pass a hashable list
to the Counter class and it will do the magic. Unfortunately dictionaries aren't hashable
so we can't use this for dictionary counting.

        from collections import Counter
        words = 'All the in in in the entire world can be put into this string, hey hey hey'.split()

        counts = Counter(words)  # Count and store each word in the list along with a count of occurrences
        print(counts)

Which words were most commonly found? Easy, `.most_common()` method helps us with that.

        print(counts.most_common(3))  # Displays the top three most common occurrences

# Match on common string groups with `string` constants
To not have to remember ascii numbers manually for checks we can import the string module
It helps us by having predefined values for all things string.
We simply import the standard string module and then we're free to use pre-set strings
like ascii_uppercase, string.punctuation etc.


        import string
        
        def is_upper(word):
            for letter in word:
                if letter not in string.ascii_uppercase:
                    return False
            return True

        print('Is LOL all uppercase?', is_upper('LOL'))

        Is LOL all uppercase? True

Note how there is a built-in function `.isupper()` already that can be used right off the bat.
The difference with the built-in function is that it allows for white spaces. Our home-made
is_upper() doesn't allow for whitespaces.

        print('Is "LOL LOL" all uppercase?', is_upper('LOL LOL'))
        print('Is "LOL LOL" all uppercase?', 'LOL LOL'.isupper())
        
        Is "LOL LOL" all uppercase? False
        Is "LOL LOL" all uppercase? True
        
# Generate permutations and combinations with `itertools`

How many combinations of your friends can be next to each other on a bus ride to
the blue mountains? There are two seats next to each other on each side of the bus.
Such an easy job for itertools!

        from itertools import permutations, combinations
        
        friends = ['Ashley', 'Romy', 'Jamie', 'Dumbo', 'Alladin']

Create a permutation object. r= sets the grouping size.
Note that ['Ashley', 'Romy'] is different from ['Romy', 'Ashley']

        list_of_perm = permutations(friends, r=2)

Create a combination object. r= sets the grouping size.
Note that ['Ashley', 'Romy'] is not different from ['Romy', 'Ashley']

        list_of_comb = combinations(friends, r=2)

        print(f'objects are:\n{type(list_of_perm)}\n{type(list_of_comb)}')

To print out the values, convert to a `list()` and we count permutations and combinations with `len()`

        print(f'{len(list(list_of_perm))}\n{len(list(list_of_comb))}')

# Debug with breakpoint() instead of print() - AWESOME
print() becomes cumbersome as the code base grows. It's fine for small problems but as the scale
goes up, it's harder to justify.
Thankfully, there's a cool debugger feature built into python's standard library.
If we're on 3.7 we can simply use `breakpoint()` anywhere in the code.
if we're on <3.6 then we use:
    
    import pdb
    
    pdb.set_trace()

Regardless of method we used the interpreter will stop at the given breakpoint and send
us to the python debugger.

It's a very comprehensive list of available options in the pdb debugger:

    Documented commands (type help <topic>):
    ========================================
    EOF    c          d        h         list      q        rv       undisplay
    a      cl         debug    help      ll        quit     s        unt      
    alias  clear      disable  ignore    longlist  r        source   until    
    args   commands   display  interact  n         restart  step     up       
    b      condition  down     j         next      return   tbreak   w        
    break  cont       enable   jump      p         retval   u        whatis   
    bt     continue   exit     l         pp        run      unalias  where    

## Debug Console
In the debugger we get a much-like-ipython interactive interpreter to work with, at the given
breakpoint in our code.
Simply submit the name of a variable and it will give you the content.

        (Pdb) pages
        ['https://www.lipsum.com/', 'https://app.statuscake.com/Workfloor/Locations.php?format=json']
        
Give it whatis <argument> and it will give you the type of your argument

        (Pdb) whatis pages
        <class 'list'>
        
        (Pdb) whatis debug_printer
        Function debug_printer
        
Submit whatis <function/class>() just like calling a function or a class and it will do it for you!
Amazing! It will also print out the type of the return value for you!
        
        def debug_printer():
            print("Hello there, I'm a function. I've been executed just now. I'll return a False now.")
            return False
    
        (Pdb) whatis debug_printer()
        Hello there, I'm a function. I've been executed just now. I'll return a False now.
        <class 'bool'>

## Debug interactive interpreter
Start an interactive interpreter whose global namespace contains all the (global and local) names
found in the current scope.
Now we're talking, almost like ipython wherever your want in your code!
        
        (Pdb) interact
        *interactive*
        
        >>> debug_printer()
        Hello there, I'm a function. I've been executed just now. I'll return a False now.
        False
        >>> newlist = [4,5,6,7]
        >>> newlist
        [4, 5, 6, 7]

## Step by Step (and list)
list and step together give a pretty amazing functionality of understanding the flow.
Submit list to see where in the code you are.
 
     (Pdb) list
     45  	def main():
     46  	    webpages = [WebPage(page) for page in pages]
     47  	    session = session_creator()
     48  	    # Practicing use of breakpoints. Submit "continue" when you're ready in the debugger.
     49  	    breakpoint()
     50  ->	    for page in webpages:
     51  	        page.fetch(session)
     52  	        print(page.get_status())
     53  	        print(page.get_status())
     54  	        print(page.get_text())
     55  	        if "json" in page.get_url():
  
Run step to execute the current line, stop at the first possible occasion (either in a
function that is called or in the current function).
  
    (Pdb) step
    > /home/par/PycharmProjects/python-2019/writing_efficient_code/breakpoint.py(51)main()
    -> page.fetch(session)

Run step again to see that we moved to the next line

    (Pdb) list
     46  	    webpages = [WebPage(page) for page in pages]
     47  	    session = session_creator()
     48  	    # Practicing use of breakpoints. Submit "continue" when you're ready in the debugger.
     49  	    breakpoint()
     50  	    for page in webpages:
     51  ->	        page.fetch(session)
     52  	        print(page.get_status())
     53  	        print(page.get_status())
     54  	        print(page.get_text())
     55  	        if "json" in page.get_url():
     56  	            page.convert_json()
 
Step one step further to call the "fetch" function in this case 
 
    (Pdb) step
    --Call--
    > /home/par/PycharmProjects/python-2019/writing_efficient_code/breakpoint.py(11)fetch()
    -> def fetch(self, session):

Ensure we moved into the function by executing list

    (Pdb) list
      6  	    def __init__(self, url):
      7  	        self.url = url
      8  	        self.text = ""
      9  	        self.status = None
     10  	
     11  ->	    def fetch(self, session):
     12  	        response = session.get(url=self.url)
     13  	        self.text = response.text
     14  	        self.status = response.status_code
     15  	
     16  	    def convert_json(self):
    (Pdb)      
    
If we would encounter an error it will throw an error to the console and you'll stay at the
same line as you were. It seems to progress after executing "step" twice though?
I simulated an error here by renaming a function to something that doesn't exist:

    (Pdb) list
     45  	def main():
     46  	    webpages = [WebPage(page) for page in pages]
     47  	    session = session_creator()
     48  	    # Practicing use of breakpoints. Submit "continue" when you're ready in the debugger.
     49  	    breakpoint()
     50  ->	    for page in webpdages:
     51  	        page.fetch(session)
     52  	        print(page.get_status())
     53  	        print(page.get_status())
     54  	        print(page.get_text())
     55  	        if "json" in page.get_url():  
    
    
    (Pdb) step
    NameError: name 'webpdages' is not defined
    > /home/par/PycharmProjects/python-2019/writing_efficient_code/breakpoint.py(50)main()
    -> for page in webpdages: