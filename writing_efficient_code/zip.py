# Zip return a generator-like zip object.
# The .__next__() method returns a tuple from the i-th element
# The .__next__() method continues until the shortest iterable in the argument sequence
# is exhausted and then it raises StopIteration.

mykeys = [1, 2, 3, 4, 5]
myvalues = ['one', 'two', 'three']
myzipobject = zip(mykeys, myvalues)

# Can be generated and exhausted once. Zip returns tuples!
print(f"zip object exhausted into list: {list(myzipobject)}")  # Generates a list of tuples
# But not again!
print(f"zip object is already exhausted, should return nothing: {list(myzipobject)}")

# zip() is particularly useful when creating dictionary comprehensions
# Set the stage with two lists of arbitrary values
mykeys = ['mykey' + str(x) for x in range(1, 6)]
myvalues = ['myvalue' + str(x) for x in range(1, 6)]

# And here's the dictionary comprehension! We're generating tuple pairs with the zip.
# We're exhausting the zip object to the lowest common denominator into the dictionary with the comprehension
mydictionary = {key: value for key, value in zip(mykeys, myvalues)}
print(f'mydictionary is: {mydictionary}')

# You can use zip to package more arguments into the zip object
more_values = 'These are more values'.split()  # Creating a list of words in strings
print(f'zip can create triple tuples too: {list(zip(mykeys, myvalues, more_values))}')