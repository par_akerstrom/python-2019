# Construct a simple list
numbers = [1, 2, 3, 4, 5, 6, 7]

# Select a portion of the list with syntax list[start:stop]
print(f'0:3 of numbers is: {numbers[0:3]}')
print(f'4:<end of list> of numbers is: {numbers[4:]}')

# We can concatenate the two prior slices, so smart way!
numbers_shifted_left = numbers[4:] + numbers[:4]
print(numbers_shifted_left)

""" NESTED LISTS (why would we ever want them though? """

# We can unpack nested lists with for loops like this:
marklist = [['Barry', 21], ['Harry', 25], ['Paula', 39]]

for name, score in marklist:
    print(name, score)

""" REPLACE MAP """


def square(x):
    return x * x


# With a map
print(list(map(square, numbers)))

# With a list comprehension
print([square(x) for x in numbers])

""" REPLACE FILTER """


def is_odd(x):
    return bool(x % 2)


# With a filter
print(list(filter(is_odd, numbers)))

# With a list comprehension
print([x for x in numbers if is_odd(x)])

""" MULTI-ITERATION LIST COMPREHENSION """
# Not sure how readable this is though....

x, y, z, n = 1, 1, 1, 2  # Our input, four integers.

mylist = [
    [i, j, k]
    for i in range(x + 1)  # Iterates through range of x
    for j in range(y + 1)  # Iterates through range of y
    for k in range(z + 1)  # Iterates through range of z
    if i + j + k != n]  # Only if i+j+k is not equal to n
print(f'mylist is {mylist}')
