
import asyncio
import json
import time
import aiohttp
from random import randint


async def worker(name, numbers, session):
    """
    :param name: name of the worker
    :param numbers: amount of random numbers
    :param session: the http session
    :return: coroutine object
    """
    print(f'worker-{name}')
    # Disabling below url because the webpage is sooo slow (probably others ddosing it)
    # url = f'https://qrng.anu.edu.au/API/jsonI.php?length={numbers}&type=uint16'
    url = f'https://www.random.org/integers/?num={numbers}&min=1&max=6&col=1&base=10&format=plain&rnd=new'
    response = await session.request(method='GET', url=url)
    value = await response.text()
    # Commenting out the below as new webpage gives a string back instead of json
    # value = json.loads(value)  # Load string to json
    # return sum(value['data'])
    return value


def gen_workers(stop):
    """

    :param stop: End to the generator, sets the amount of workers
    :return: generator object
    """
    for member in range(1, stop + 1):
        yield member, randint(1, 10)


async def main():
    """
    This async function
    :return:
    """
    async with aiohttp.ClientSession() as session:
        # Asynchronous by manually explicitly putting each request in asyncio.gather()
        responses = await asyncio.gather(
            worker('bob', 3, session), worker('alice', 3, session),
            worker('foo', 3, session), worker('bar', 3, session),
        )
        print(responses, type(responses))

        # Asynchronous with generator expression and unpacking
        sums = await asyncio.gather(
            *(worker(f'w-{i}', n, session) for i, n in enumerate(range(1, 2)))
        )
        print(sums)

        # Asynchronous with iteration over iterable from generator function
        members = list(gen_workers(5))  # Exhaust generator and create list
        tasks = []
        for member in members:  # Iterate over list
            task = asyncio.ensure_future(worker(member[0], member[1], session))
            tasks.append(task)
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        print(responses)


async def alternate_worker_pool():
    async with aiohttp.ClientSession() as session:
        # Asynchronous with iteration over iterable from generator function
        members = list(gen_workers(2))
        tasks = []
        for member in members:
            task = asyncio.ensure_future(worker(member[0], member[1], session))
            tasks.append(task)
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        print(responses)


if __name__ == '__main__':
    # Run the alternate worker pool, just to show that we can have two event loops
    start = time.perf_counter()
    asyncio.run(alternate_worker_pool())
    elapsed = time.perf_counter() - start
    print(f'executed generator in {elapsed:0.2f} second/s')

    # Run the larger, main() pool with manual, expression and generator function
    start = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - start
    print(f'executed generator expression and manual task in {elapsed:0.2f} second/s')