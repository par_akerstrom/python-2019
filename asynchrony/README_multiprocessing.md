# Multiprocessing
Multiprocessing is, unlike asyncio and threading, using multiple CPUs.

## Heavy Starting of multiple interpreters
What multiprocessing does is creating a new instance of the python interpreter to run
on each CPU and then farming out part of your program to run on the CPUs.

Starting multiple interpreters is obviously a bit more taxing than just running a new thread.
However, with the right design for the right problem, it can be very advantageous.

![multiprocessor](multiprocessor.png)

Not sure why, but the file snatched from realpython doesn't run in my standard interpreter.
I get the following AttributeError:

        File "multiprocessing.py", line 21, in download_all_sites
        with multiprocessing.Pool(initializer=set_global_session) as pool:
        AttributeError: module 'multiprocessing' has no attribute 'Pool'

It however runs fine in ipython.

        Python 3.6.7 (default, Oct 22 2018, 11:32:17) 
        Type 'copyright', 'credits' or 'license' for more information
        IPython 7.5.0 -- An enhanced Interactive Python. Type '?' for help.
        ForkPoolWorker-3:Read 273 from http://olympus.realpython.org/dice
        ForkPoolWorker-4:Read 273 from http://olympus.realpython.org/dice
        ForkPoolWorker-6:Read 273 from http://olympus.realpython.org/dice
        ForkPoolWorker-2:Read 273 from http://olympus.realpython.org/dice
        ForkPoolWorker-4:Read 273 from http://olympus.realpython.org/dice
        ForkPoolWorker-1:Read 19210 from https://www.jython.org
        ForkPoolWorker-8:Read 19210 from https://www.jython.org
        ForkPoolWorker-5:Read 19210 from https://www.jython.org
        ForkPoolWorker-7:Read 19210 from https://www.jython.org
        ForkPoolWorker-3:Read 19210 from https://www.jython.org
        Downloaded 10 in 1.6239283084869385 seconds
