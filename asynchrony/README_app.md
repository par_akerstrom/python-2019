# Requirements / Use
Asynchronously call an online rest api to generate random numbers.
Values are returned as json. Convert the json to a dictionary. Filter out the random
integers and sum them up. Print the sum and mark the time it took.
This is the website we're using to generate the random numbers:
    
    https://qrng.anu.edu.au/API/jsonI.php?length=20&type=uint16

# Modules
We use asyncio and aoihttp for asynchronous capabilities

# Design
## Main and Workers (async func, returning coroutine)
main() will create an http session for us but should not be cluttered with the external calls.

We'll create a worker asynchronous function that will return a coroutine (can sort of be seen
as spawning workers). Note how the async function is still synchronous in python as python
only has one thread, BUT the external call are concurrent (at least overlapping).

We start with these two functions blank and we run and time it all by calling the main
module.

        async def worker(name, numbers, session):
            pass
        
        async def main():
            await asyncio.sleep(1)
            pass

        if __name__ == '__main__':
            start = time.perf_counter()
            asyncio.run(main())
            elapsed = time.perf_counter() - start
            print(f'executed in {elapsed:0.2f} seconds')
            
         executed in 1.00 seconds

# aiohttp session
So let's get onto creating our http session call in main().
It's best practice to use "with" when managing sessions and files. So let's create one in
our main function:

        async def main():
            with aiohttp.ClientSession() as session:
                await asyncio.sleep(1)

            TypeError: Use async with instead
            Unclosed client session

Uh oh, that didn't work well. The reason is that since we're using the aiohttp module's
async ClientSession() function we need to define the with statement as asynchronous.

        async def main():
            async with aiohttp.ClientSession() as session:
                await asyncio.sleep(1)
        pass
        
        executed in 1.00 seconds

That worked better, great.
Let's make a single static request for now. We define worker name as bob, we request 3
random numbers and we attach the aiohttp async session.
We will attempt to print the response and the response type.

        async def main():
            async with aiohttp.ClientSession() as session:
            response = await worker('bob', 3, session)
            print(response, type(response))


So now let's make our worker pool do something with this call. 
We will print out the worker's name, we will construct a url, we will send a HTTP GET
request to the url, capture the json response and convert it into a dictionary.
Lastly we will return the json dictionary to the caller.

        print(f'worker-{name}')
        url = f'https://qrng.anu.edu.au/API/jsonI.php?length={numbers}&type=uint16'
        response = await session.request(method='GET', url=url)
        value = await response.text()
        value = json.loads(value)  # Load jason string to dictionary
        return value
        
Cool, let's run it. We should get the name of the worker and a dictionary with 
three random numbers and some other data.

        In [8]:     async with aiohttp.ClientSession() as session: 
        ...:         response = await worker('bob', 3, session) 
        ...:                                                                                                  
        worker-bob                                                                                            

        In [9]: response?                                                                                        
        Type:        dict
        String form: {'type': 'uint16', 'length': 3, 'data': [50714, 27224, 20255], 'success': True}
        
Fantastic.
So remember how we must sum the values in the dictionary? First let's filter them out
in the dictionary through getting the key 'data'. Use sum() to sum them up

        return sum(['data'])

        worker-bob
        115932 <class 'int'>
        executed in 0.47 seconds

Awesome, now let's run this way more times than just one to make concurrency obvious.

# Generating many worker calls
Let's start out super easy with our event loop utilising asyncio.gather() that let's us 
manually define multiple calls. We define four manual calls that can be paused while 
waiting for the external operation to complete.

        async def main():
            async with aiohttp.ClientSession() as session:
                responses = await asyncio.gather(
                worker('bob', 3, session), worker('alice', 3, session),
                worker('foo', 3, session), worker('bar', 3, session),
                )
                print(responses, type(responses))
                
        worker-bob
        worker-alice
        worker-foo
        worker-bar
        executed in 0.89 seconds

Easy, this successfully pauses the operation when waiting for the trigger callback
The four requests and sends them all more or less concurrently.
               

But how about when we want to do let's say 10 requests? That's a lot of manual typing.
I first thought that creating a generator function and iterating over it would be a good
idea. It would take number of workers and generate a random number to look for, for each
member. We loop over the generator and send the yielded responses to the worker pool.

        def gen_workers(stop):
            for member in range(1, stop + 1):
            yield member, randint(1, 10)
        
        members = list(gen_workers(10))  # Exhaust the generator
        for member in members:  # Loop over our list
            # pausable function in our event loop
            responses = await asyncio.gather(worker(member[0], member[1], session))
            print(responses)  # process doesn't get here until above is completed.

Not super pythonic but it works to generate the worker id's and a random number of random
numbers to request from the webpage.
The biggest problem with this is that it's actually not async. The for loop never gets 
looped until a response has actually reached the event loop and we trigger a callback.
At least I think that's what's happening.

So how can we utilise a generator whilst keeping it asynchronous? Remember how we couldn't
hash a list comprehension for async, but we can create and unpack a generator expression? 
Reminder, the generator expression and unpacking looks like *(generator expression)
We will use the function enumerate to create an automatic "index" for each of the values
generated by our generator.

        # Asynchronous way with generator expression and unpacking
        sums = await asyncio.gather(
            *(worker(f'w-{i}', n, session) for i, n in enumerate(range(1, 20)))
        )
        print(sums)

        worker-w-0
        worker-w-1
        worker-w-2
        ...
        worker-w-17
        worker-w-18
        [55493, 109814, 109542, 124589, 236704, 87563, 216455, 214408, 368036, 344285,
        363239, 400464, 375639, 468059, 356659, 689541, 602930, 776995, 471662]

So what does the above say?
We store all answers in a list, sums.
We use our event loop to wait for the asynchronous gathering of callbacks with asyncio.gather
We unpack the generator expression with the *.
The worker pool is called by our generator. The generator constructs a series of numbers
with the range function. The enumerate portion adds an index to each of the numbers.
So basically i = index (worker name). We use f-string to send name w-i
n = number (number of numbers to get from the webpage)
session = our aiohttp async http session 