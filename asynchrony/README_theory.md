# %time
Note how this markdown has measured code execution time statements throughout.
These are done via iPython's magic commands.
        
        %time:
        Time execution of a Python statement or expression.
        The CPU and wall clock times are printed, and the value of the expression (if any) is returned.
        
Example is this, let's run our synchronous function that sleeps for three seconds.
We run it three times with a list comprehension. We see the execution time is 9.01s.

        In [1]: %time [randn() for i in range(3)]                                                               
        CPU times: user 2.24 ms, sys: 177 µs, total: 2.42 ms
        Wall time: 9.01 s
        
# asyncio module
Used to write concurrent code using the async/await syntax. Usually a perfect fit for any I/O bound code.
If your code is CPU bound, consider using the multiprocessing library instead.        
     

# Generators (yield product)
Produces sequence of values. Sequence of integers, floats, booleans. Any value really.
yield is the keyword for a generator. It will return a generator object (iterable).

Let's create a generator function

        def odds(start, stop):
        # +1 to include the stop value, 2 for every second value
        for odd in range(start, stop + 1, 2):
        yield odd # return out the first value, then pause

Running this function doesn't return a value straight away.
Instead, it returns a generator object that will later give you values.
We call it g for generator_object :)

        In [3]: g = odds(1,15)
        In [4]: g
        Out[4]: <generator object odds at 0x7f5ff9cab9b0>

With this generator object we can now run a couple of built-in functions, such as next().
next() gives us the next value in the generator, until we have exhausted the generator.
We have exhausted the generator when we git the stop value.
At the stop, we will get an StopIteration error thrown. The generator is now stuck at
the StopIteration error and can't generate more values.

        In [5]: next(g)
        Out[5]: 1
        In [6]: next(g)
        Out[6]: 3  
        ---
        In [13]: next(g)
        ---------------------------------------------------------------------------
        StopIteration                             Traceback (most recent call last)
        <ipython-input-13-e734f8aca5ac> in <module>()
        
It's tedious to write next, next, next all the time. We can be a bit more efficient
and automatically exhaust the generator instead.
We can do this with list(generator). (or of course with tuple()!)
So we create a new generator (g2) and then perform list(g2). The list function will
call next, next, next for you until exhaustion and provide you with the resulting list.

        In [1]: g2 = odds(1, 20)
        In [2]: g2?
        Type:        generator
        String form: <generator object odds at 0x7fd324f935a0>
        Docstring:   <no docstring>
        
        In [3]: list(g2)
        Out[3]: [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
 
 Another way of exhausting the generator is with an iterating function like a for loop.
 We create a new generator (g3) and iterate through the values with for, like this:
 
        In [1]: g3 = odds(3, 13)
        
        In [2]: g3?
        Type:        generator
        String form: <generator object odds at 0x7f6eb1412640>
        Docstring:   <no docstring>
        
        In [3]: for value in g3:
           ...:     print(value)
           ...:     
        3
        5
        7
        9
        11
        13

Or why not make it even simpler with a list comprehension to generate a list straight away?
We do this by looping over the values in the generator object from odds(3, 15).
Voila! We get a list back.

        In [4]: odd_values = [odd for odd in odds(3, 15)]

        In [5]: odd_values
        Out[5]: [3, 5, 7, 9, 11, 13, 15]

# Couroutines (asynchronous function product)
Coroutines are similar to generators in the sense that they can be started, paused,
restarted and stopped.
But they're different in that they consume values instead of generating values!

Let's create a coroutine function, we do this with async, asyncio and await.
We make the sleep function asynchronous with asyncio.sleep().
We are then forced to put await in front of asyncio.
What await does is that it allows for the asynchronous function to pause, while waiting
for a Callback Trigger (from the event loop)
Other code in our app will still run in the background! Like our main function will continue
to happily run.

        def randn_async():
        await asyncio.sleep(3) # await for outbound request(sleep) before continuing
        return randint(1, 10)

So we can run this now right? No!
Note how the whole function must be defined as async for us to be able to use the await
and async keywords.
        
        SyntaxError: 'await' outside async function

So what we do is we add "async" in front of the function definition to allow it to be paused.

        async def randn_async(): # Asynchronous function
        await asyncio.sleep(3) # await for outbound request(sleep) before continuing
        return randint(1, 10)      

        
When we execute this function we get a coroutine object back, instantaneously.
I suppose the coroutine is ready to consume values.

        In [2]: %time randn_async()                                                                              
        CPU times: user 4 µs, sys: 0 ns, total: 4 µs
        Wall time: 6.91 µs
        Out[2]: <coroutine object randn_async at 0x7f1271ff7db0>


So how can we call this function? Just right off the bat? No!
When we try to run it, we get this error:

        main():
        r = randn()
        
        theory.py:40: RuntimeWarning: coroutine 'randn_async' was never awaited

Ah, that's right, anything that is asynchronous must be allowed to be paused.
We allow for the function to be paused with await. But uh oh, new error!

        main():
        r = await randn() # pausable call to randn()
        
        SyntaxError: 'await' outside async function
        
Now that we enabled async in our main function, it must be asynchronous.
We reconfigure our main function with async def main().

        async def main()
        r = await randn() # pausable call to randn()
        
        theory.py:40: RuntimeWarning: coroutine 'main' was never awaited
        main()

Oh, that's right, now our call to main() must be pausable, with await!
With that, we're getting to the event loop.

![Event Loop](asyncio.png)

# Event Loop
Event loops are the core of every asyncio application.
They run async tasks, trigger callbacks and run I/O operations such as network tasks.

The event loop is aware of each task and knows what state it is in. The event loop controls how and when each task is run. A simplified view of states is that two states exist; ready state and waiting state. There are more states but let’s skip that for now.

The event loops maintains two lists, one for each of the tasks states. The event loop selects one of the ready tasks and starts it (effectively placing it in the waiting queue I suppose). The task is now running independently until it cooperatively hands the control back to the event loop.
The event loop puts the task either in the ready or in the waiting queue.
It then scans the waiting queue to see if any task has become ready by having its I/O operation completing. After sorting the tasks it picks a new task to run, likely the task that has been waiting the longest, and so it repeats.

The ready state indicates that a task has work to do and that it can be run.
The waiting state indicates that the task is waiting for something external to finish, such as a network operation.

In the event loop code the requests are sent outbound towards the destination operation. This is generally something I/O bound, externally to python. This could be database requests, http requests etc. These requests may be concurrent as the operation may take longer time to process than it takes to send out the next request.
Once the operating system is done with the external task it let’s python know that it is done via an Operation Complete command.
The event loop fires off a Trigger Callback with a reference to the original request.


We create our event loop in our if statement for running main.

        if __name__ == "__main__": # if the name of this module is main, do this
        asyncio.run(main()) # Asynchronous run of main(). Our Event Loop.
        
OK so now with our event loop for our main function we can run this beast.
We create a little time catching function to measure execution time.
Let's start with a synchronous call. We still need to have await in the call for
us to be able to execute. But it's not async yet.

        start = time.perf_counter()
        r = await randn_async()
        elapsed = time.perf_counter() - start
        print(f'{r} took {elapsed:0.2f} seconds in synchronous call.')

        1 took 3.00 seconds in synchronous call.

Okay cool that's working well. Let's try to make an asynchronous call now.
We can do this with asyncio.gather(). gather() let's us define multiple calls to the 
coroutine destination, our async function.

    start = time.perf_counter()
    r = await asyncio.gather(randn_async(), randn_async(), randn_async())
    elapsed = time.perf_counter() - start
    print(f'{r} took {elapsed:0.2f} seconds in asynchronous call')
    
    [2, 6, 2] took 3.00 seconds in asynchronous call

Wow, so three values took just as long time as 1 value!
That's awesome, so what if we do it for 10 values?
Let's try it with a list comprehension, easy right.

        r = await asyncio.gather([randn_async() for _ in range(10)])
        
        TypeError: unhashable type: 'list'

Uh oh, we can't hash a list for the coroutine.
What about a generator? That must be a good way for our coroutine that wants to consume
values right?
We can define a generator with syntax looking much like a tuple comprehension.
All we do is replace [] with (), this turns our list comprehension into a generator.

        r = await asyncio.gather((randn_async() for _ in range(10)))

        RuntimeError: Task got bad yield: <coroutine object randn_async at 0x7fd8a8cb6148>

Uh oh, that didn't work either. That's because we need to unpack the values that are 
returned to us from the coroutine.
We unpack with a preceding asterisk in front of the generator object.    

        r = await asyncio.gather(*(randn_async() for _ in range(10)))

        [6, 6, 10, 3, 6, 4, 9, 3, 6, 9] took 3.00 seconds in asynchronous call
 
Boom, 10 values at the same speed of 1 value! Note that all these calls are still in
a single thread. What we've done is just allow pausing whilst waiting for the external call
(which is the sleep) and continue on other work in the meantime.

Now it's time to look at how we can combine a generator and being asynchronous at the same
time. We do this with Asynchronous Generators.

# Asynchronous Generators
A combination of a generator (generating values) and a asynchronous function (consuming
values with its coroutine), maintaining the ability to start, pause, restart and stop.

So naturally we create an asynchronous function as we want to be able to consume values 
asynchronously. 

        async def square_odds(start, stop):

We reuse our old generator function to help us generate values from our start and 
stopping point. 

        for odd in odds(start, stop):  # Pass start and stop to generator and iterate over it

We simulate an external function that takes time, perhaps a database call or a network call
        
        await asyncio.sleep(2)  # simulation of external function that takes time
        
Lastly, we use the yield keyword to return a generator. But this time around it's an
async generator as it comes from an async funcion!

        yield odd ** 2  # returns odd value to the power of two
    
Putting it all together and running it

        async def square_odds(start, stop):
            for odd in odds(start, stop):  # Pass start and stop to generator and iterate over it
            await asyncio.sleep(2)  # simulation of external function that takes time
            yield odd ** 2  # returns odd value to the power of two     

# Calling Asynchronous Generators
We can't use the previously used simple await to call the async generator.
It gives us a TypeError.

        In [8]: r = await square_odds(1,3)                                                                       
        TypeError: object async_generator can't be used in 'await' expression

We can't use the previously used asyncio.gather() either. That too is a TypeError

        await asyncio.gather(square_odds(1,3))
        TypeError: An asyncio.Future, a coroutine or an awaitable is required

We can however simply call the function and store the async_generator object

        In [9]: async_gen = square_odds(1, 9)                                                                    
        In [10]: async_gen?                                                                                      
        Type:        async_generator
        String form: <async_generator object square_odds at 0x7f9df16128d0>
        Docstring:   <no docstring>

# Unpacking Asynchronous Generator Objects
Now that we have our async_generator object returned from our function, how do we unpack it?
We can't just iterate over the object like normal, because it's no iterable.

        for line in async_gen:
            print(line)
        TypeError: 'async_generator' object is not iterable

As the returned object will be async we need to iterate over it with an async enabled loop.
We can do this with "async for"


        In [11]: async for line in async_gen: 
            ...:     print(line)                                                                                 
            1
            9
            25
            49
            81

We can of course do this straight on the async function call as well:

        start = time.perf_counter()
        async for so in square_odds(1, 9):  # Async for loop
            print(f'square odd {so}')
        elapsed = time.perf_counter() - start
        print(f'async_generator took {elapsed:0.2f} seconds in async call')
                
        square odd 1
        square odd 9
        square odd 25
        square odd 49
        square odd 81
        async_generator took 15.02 seconds in async call
    
Note how the execution needs to wait for the async generator function's external
call (sleep for 3 seconds). This is different than the straight on generator function.

# async and await
Await is sort of like magic that allows the task to hand control back to the event loop.
When your code awaits a function call, it’s a signal that this may take a while,
and the task should give control to the event loop.

Async is like a flag to Python telling it that the function that is about to be
defined will use await somewhere in it. This is not true in all cases
(for example asynchronous generators and async with), but for most cases it is true.