from random import randint
import time
import asyncio


def odds(start, stop):
    """
    :return: generator object
    """
    # +1 to include the stop value, 2 for every second value
    for odd in range(start, stop + 1, 2):
        yield odd  # return out the first value, then pause


def randn(): # synchronous function
    time.sleep(3) # Sleep for three seconds
    return randint(1, 10)  # Return random int between 1 and 10


async def randn_async():  # Asynchronous function
    """
    :return: coroutine object
    """
    await asyncio.sleep(3)  # await for outbound request(sleep) before continuing
    return randint(1, 10)


async def square_odds(start, stop):
    """
    :return: async generator object
    """
    for odd in odds(start, stop):  # Pass start and stop to generator and iterate over it
        await asyncio.sleep(3)  # simulation of external function that takes time
        yield odd ** 2  # returns odd value to the power of two


async def main():
    # list comprehension to generate a list with our generator odds
    odd_values = [odd for odd in odds(3, 15)]
    odd_values_in_tuple = tuple(odds(10, 20))
    print(odd_values)
    print(odd_values_in_tuple)

    start = time.perf_counter()
    r = await randn_async()
    elapsed = time.perf_counter() - start
    print(f'{r} took {elapsed:0.2f} seconds in synchronous call.')

    start = time.perf_counter()
    # generator of 1-10, unpacked and async sent to randn_async function
    r = await asyncio.gather(*(randn_async() for _ in range(10)))
    elapsed = time.perf_counter() - start
    print(f'{r} took {elapsed:0.2f} seconds in asynchronous call')

    start = time.perf_counter()
    async for so in square_odds(1, 9):  # Async for loop
        print(f'square odd {so}')
    elapsed = time.perf_counter() - start
    print(f'async_generator took {elapsed:0.2f} seconds in async call')

if __name__ == "__main__":  # if the name of this module is main, do this
    # main() # Synchronous run of main
    asyncio.run(main())  # Asynchronous run of main. Our Event Loop.
