import matplotlib.pyplot as plt

labels = 'Python', 'C++', 'Ruby', 'Java', 'PHP', 'Perl'  # tuple
sizes = [33, 52, 12, 17, 62, 48]  # list
separated = (0.1, 0, 0, 0, 0, 0)  # tuple

plt.pie(sizes, labels=labels, autopct='%1.1f%%', explode=separated)  # pie diagram (value, description)
# autopct='%1.1f%%' is a stupid value to set the percentages
# explode=(tuple) uses the tuple to set values to be "exploded"

plt.show()  # show our plot