import matplotlib.pyplot as plt
import pandas

# Panda cheat sheet: https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf

# Phone support with resolved issues case
# old school way of defining, so tedious.
data = [{
            'Name': 'Nick',
            'jan_issues_resolved': 124,
            'feb_issues_resolved': 100,
            'march_issues_resolved': 165
        },
        {
            'name': 'Panda',
            'jan_issues_resolved': 112,
            'feb_issues_resolved': 80,
            'march_issues_resolved': 3
        },
        {
            'Name': 'Valos',
            'jan_issues_resolved': 124,
            'feb_issues_resolved': 100,
            'march_issues_resolved': 165
        },
        {
            'name': 'Roger',
            'jan_issues_resolved': 112,
            'feb_issues_resolved': 80,
            'march_issues_resolved': 3
        }]

# pandas way of doing it
raw_data = {'names': ['Nick', 'Panda', 'Valos', 'Roger'],
            'jan_issues_resolved': [124, 112, 124, 112],
            'feb_issues_resolved': [100, 80, 100, 80],
            'march_issues_resolved': [165, 3, 165, 3]}

# We can pass this data into pandas to create a data frame

df = pandas.DataFrame(raw_data, columns=['names', 'jan_issues_resolved',
                                         'feb_issues_resolved', 'march_issues_resolved'])

# instead of a for loop in the old school way we can now do a simple add like this:
df['total_issues_resolved'] = (df['jan_issues_resolved'] +
                               df['feb_issues_resolved'] +
                               df['march_issues_resolved'])
color = [(1, 0.4, 0.4), (1, 0.6, 1), (0.5, 0.3, 1), (0.3, 1, 0.5), (.7, .7, .2)]  # Pretty silly color format tbh. It's RGB divided by 255

# Print pandas data format
print(df)

# Construct a piechart with pyplot, with data from pandas
plt.pie(df['total_issues_resolved'],
        labels=df['names'],
        colors=color,
        autopct='%1.1f%%')

plt.show()