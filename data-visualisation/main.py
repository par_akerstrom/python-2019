import matplotlib.pyplot as plt

# Years since Christ
years = [1950, 1955, 1960, 1965, 1970, 1975, 1980,
         1985, 1990, 1995, 2000, 2005, 2010, 2015]

# World population
pops = [2.525, 2.750, 3.018, 3.322, 3.682, 4.061, 4.440, 4.853,
        5.310, 5.735, 6.127, 6.520, 6.930, 7.439]

deaths = [1.2, 1.7, 1.8, 2.2, 2.5,
          2.7, 2.9, 3, 3.1, 3.3, 3.5, 3.8, 4.0, 4.3]

births = [1.3, 1,8]

# plot has plenty of kwargs - see https://matplotlib.org/api/_as_gen/matplotlib.pyplot.plot.html
plt.plot(years, pops, '--', color=(255/255, 100/255, 100/255))  # pyplot.plot(x axis, y axis)
plt.plot(years, deaths, color=(0.6, 0.6, 1))
plt.ylabel("Population in billions")
plt.xlabel("Population growth by year")
plt.title("Population growth")
plt.show()
