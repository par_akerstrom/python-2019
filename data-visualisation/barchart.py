import matplotlib.pyplot as plt
import numpy as np

col_count = 3
bar_width = 0.2

# math, reading, science
korea_scores = (554, 536, 538)
canada_scores = (518, 523, 525)
china_scores = (613, 570, 580)
france_scores = (495, 505, 499)

index = np.arange(col_count)

k1 = plt.bar(index, korea_scores, bar_width,
             alpha=0.4, label="Korea")  # alpha sets softer colours
c1 = plt.bar(index + 0.2, canada_scores, bar_width,
             alpha=0.4, label="Canada")
ch1 = plt.bar(index + 0.4, china_scores, bar_width,
              alpha=0.4, label="China")
f1 = plt.bar(index + 0.6, france_scores, bar_width,
             alpha=0.4, label="France")

def create_labels(data):
    for item in data:
        height = item.get_height()  #
        plt.text(item.get_x() + item.get_width() / 2,
                 height*1.05,
                 f'{height}',
                 ha="center", va="bottom")  # (x pos, y pos, value, formatting)

create_labels(k1)
create_labels(c1)
create_labels(ch1)
create_labels(f1)

plt.ylabel("Mean score in PISA 2012")
plt.xlabel("Subjects")
plt.title("Test Scores by Country")
plt.xticks(index + 0.3 / 2, ("Mathematics", "Reading", "Science"))  # customise x-axis ticks
plt.legend(frameon=False, bbox_to_anchor=(1, 1))  # Shows labels, bbox it outside the chart
plt.grid(True)

plt.show()